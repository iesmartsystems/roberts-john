using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_STRING_SELECTOR_V0_1
{
    public class UserModuleClass_IESS_STRING_SELECTOR_V0_1 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> SELECTSOURCE;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> SOURCEINPUT;
        Crestron.Logos.SplusObjects.AnalogOutput OUTPUTSOURCEANALOG;
        Crestron.Logos.SplusObjects.StringOutput OUTPUTSOURCESTRING;
        ushort GVSELECTSOURCE = 0;
        object SELECTSOURCE_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 74;
                GVSELECTSOURCE = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
                __context__.SourceCodeLine = 75;
                OUTPUTSOURCESTRING  .UpdateValue ( SOURCEINPUT [ GVSELECTSOURCE ]  ) ; 
                __context__.SourceCodeLine = 76;
                OUTPUTSOURCEANALOG  .Value = (ushort) ( Functions.Atoi( SOURCEINPUT[ GVSELECTSOURCE ] ) ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object SOURCEINPUT_OnChange_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            ushort LVINDEX = 0;
            
            
            __context__.SourceCodeLine = 81;
            LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
            __context__.SourceCodeLine = 82;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GVSELECTSOURCE == LVINDEX))  ) ) 
                { 
                __context__.SourceCodeLine = 84;
                OUTPUTSOURCESTRING  .UpdateValue ( SOURCEINPUT [ LVINDEX ]  ) ; 
                __context__.SourceCodeLine = 85;
                OUTPUTSOURCEANALOG  .Value = (ushort) ( Functions.Atoi( SOURCEINPUT[ LVINDEX ] ) ) ; 
                } 
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    
    SELECTSOURCE = new InOutArray<DigitalInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SELECTSOURCE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( SELECTSOURCE__DigitalInput__ + i, SELECTSOURCE__DigitalInput__, this );
        m_DigitalInputList.Add( SELECTSOURCE__DigitalInput__ + i, SELECTSOURCE[i+1] );
    }
    
    OUTPUTSOURCEANALOG = new Crestron.Logos.SplusObjects.AnalogOutput( OUTPUTSOURCEANALOG__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( OUTPUTSOURCEANALOG__AnalogSerialOutput__, OUTPUTSOURCEANALOG );
    
    SOURCEINPUT = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCEINPUT[i+1] = new Crestron.Logos.SplusObjects.StringInput( SOURCEINPUT__AnalogSerialInput__ + i, SOURCEINPUT__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( SOURCEINPUT__AnalogSerialInput__ + i, SOURCEINPUT[i+1] );
    }
    
    OUTPUTSOURCESTRING = new Crestron.Logos.SplusObjects.StringOutput( OUTPUTSOURCESTRING__AnalogSerialOutput__, this );
    m_StringOutputList.Add( OUTPUTSOURCESTRING__AnalogSerialOutput__, OUTPUTSOURCESTRING );
    
    
    for( uint i = 0; i < 16; i++ )
        SELECTSOURCE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( SELECTSOURCE_OnPush_0, false ) );
        
    for( uint i = 0; i < 16; i++ )
        SOURCEINPUT[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( SOURCEINPUT_OnChange_1, false ) );
        
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_STRING_SELECTOR_V0_1 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint SELECTSOURCE__DigitalInput__ = 0;
const uint SOURCEINPUT__AnalogSerialInput__ = 0;
const uint OUTPUTSOURCEANALOG__AnalogSerialOutput__ = 0;
const uint OUTPUTSOURCESTRING__AnalogSerialOutput__ = 1;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
