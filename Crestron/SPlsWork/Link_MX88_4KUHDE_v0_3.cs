using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_LINK_MX88_4KUHDE_V0_3
{
    public class UserModuleClass_LINK_MX88_4KUHDE_V0_3 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput CONNECT;
        Crestron.Logos.SplusObjects.DigitalInput POLL;
        Crestron.Logos.SplusObjects.DigitalInput DEBUG;
        Crestron.Logos.SplusObjects.AnalogInput IP_PORT;
        Crestron.Logos.SplusObjects.StringInput IP_ADDRESS;
        Crestron.Logos.SplusObjects.StringInput RX;
        Crestron.Logos.SplusObjects.StringInput MANUALCMD;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> SELECT_DISPLAY;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> ROUTE_SOURCE;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> SOURCEXTODISPLAY;
        Crestron.Logos.SplusObjects.DigitalOutput CONNECT_FB;
        Crestron.Logos.SplusObjects.AnalogOutput CONNECT_STATUS_FB;
        Crestron.Logos.SplusObjects.StringOutput TX;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> SOURCEXONDISPLAY;
        SplusTcpClient SWITCHERCLIENT;
        SSWITCHER SWITCHERDEVICE;
        ushort GVDISPLAY = 0;
        ushort GVSOURCE = 0;
        private void CONNECTDISCONNECT (  SplusExecutionContext __context__, ushort LVCONNECT ) 
            { 
            short LVSTATUS = 0;
            
            
            __context__.SourceCodeLine = 85;
            if ( Functions.TestForTrue  ( ( Functions.Not( LVCONNECT ))  ) ) 
                { 
                __context__.SourceCodeLine = 87;
                if ( Functions.TestForTrue  ( ( SWITCHERDEVICE.STATUSCONNECTED)  ) ) 
                    {
                    __context__.SourceCodeLine = 88;
                    LVSTATUS = (short) ( Functions.SocketDisconnectClient( SWITCHERCLIENT ) ) ; 
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 90;
                if ( Functions.TestForTrue  ( ( LVCONNECT)  ) ) 
                    { 
                    __context__.SourceCodeLine = 92;
                    if ( Functions.TestForTrue  ( ( Functions.Not( SWITCHERDEVICE.STATUSCONNECTED ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 94;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( SWITCHERDEVICE.IPADDRESS ) > 4 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( SWITCHERDEVICE.IPPORT > 0 ) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 96;
                            if ( Functions.TestForTrue  ( ( SWITCHERDEVICE.STATUSCONNECTREQUEST)  ) ) 
                                {
                                __context__.SourceCodeLine = 97;
                                LVSTATUS = (short) ( Functions.SocketConnectClient( SWITCHERCLIENT , SWITCHERDEVICE.IPADDRESS , (ushort)( SWITCHERDEVICE.IPPORT ) , (ushort)( 1 ) ) ) ; 
                                }
                            
                            } 
                        
                        } 
                    
                    } 
                
                }
            
            
            }
            
        private void SETDEBUG (  SplusExecutionContext __context__, CrestronString LVSTRING , ushort LVTYPE ) 
            { 
            
            __context__.SourceCodeLine = 104;
            if ( Functions.TestForTrue  ( ( DEBUG  .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 106;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVTYPE == 1))  ) ) 
                    {
                    __context__.SourceCodeLine = 107;
                    Trace( "Link RX: {0}", LVSTRING ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 109;
                    Trace( "Link TX: {0}", LVSTRING ) ; 
                    }
                
                } 
            
            
            }
            
        private void SETQUEUE (  SplusExecutionContext __context__, CrestronString LVSTRING ) 
            { 
            CrestronString LVTEMP;
            LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 511, this );
            
            
            __context__.SourceCodeLine = 115;
            SWITCHERDEVICE . TXQUEUE  .UpdateValue ( SWITCHERDEVICE . TXQUEUE + LVSTRING + SWITCHERDEVICE . ETX + "\u000B\u000B"  ) ; 
            __context__.SourceCodeLine = 116;
            while ( Functions.TestForTrue  ( ( Functions.Find( "\u000B\u000B" , SWITCHERDEVICE.TXQUEUE ))  ) ) 
                { 
                __context__.SourceCodeLine = 118;
                LVTEMP  .UpdateValue ( Functions.Remove ( "\u000B\u000B" , SWITCHERDEVICE . TXQUEUE )  ) ; 
                __context__.SourceCodeLine = 119;
                LVTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTEMP ) - 2), LVTEMP )  ) ; 
                __context__.SourceCodeLine = 120;
                LVTEMP  .UpdateValue ( LVTEMP + SWITCHERDEVICE . ETX  ) ; 
                __context__.SourceCodeLine = 121;
                if ( Functions.TestForTrue  ( ( SWITCHERDEVICE.STATUSCONNECTREQUEST)  ) ) 
                    { 
                    __context__.SourceCodeLine = 123;
                    if ( Functions.TestForTrue  ( ( SWITCHERDEVICE.STATUSCONNECTED)  ) ) 
                        {
                        __context__.SourceCodeLine = 124;
                        Functions.SocketSend ( SWITCHERCLIENT , LVTEMP ) ; 
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 127;
                    TX  .UpdateValue ( LVTEMP  ) ; 
                    }
                
                __context__.SourceCodeLine = 128;
                SETDEBUG (  __context__ , LVTEMP, (ushort)( 0 )) ; 
                __context__.SourceCodeLine = 116;
                } 
            
            
            }
            
        private void PARSEFEEDBACK (  SplusExecutionContext __context__ ) 
            { 
            CrestronString LVRX;
            CrestronString LVTRASH;
            LVRX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
            
            
            
            }
            
        private void ROUTEDISPLAY (  SplusExecutionContext __context__ ) 
            { 
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
            
            
            __context__.SourceCodeLine = 157;
            MakeString ( LVSTRING , "SET SW HDMIIN{0:d} HDMIOUT{1:d}", (short)GVSOURCE, (short)GVDISPLAY) ; 
            __context__.SourceCodeLine = 158;
            SETQUEUE (  __context__ , LVSTRING) ; 
            
            }
            
        object SWITCHERCLIENT_OnSocketConnect_0 ( Object __Info__ )
        
            { 
            SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
                CrestronString LVSTRING;
                LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
                
                
                __context__.SourceCodeLine = 181;
                SWITCHERDEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 182;
                CONNECT_FB  .Value = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 183;
                CONNECT_STATUS_FB  .Value = (ushort) ( SWITCHERCLIENT.SocketStatus ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SocketInfo__ ); }
            return this;
            
        }
        
    object SWITCHERCLIENT_OnSocketDisconnect_1 ( Object __Info__ )
    
        { 
        SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
            
            __context__.SourceCodeLine = 188;
            SWITCHERDEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 189;
            SWITCHERDEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 190;
            CONNECT_FB  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 191;
            CONNECT_STATUS_FB  .Value = (ushort) ( SWITCHERCLIENT.SocketStatus ) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SocketInfo__ ); }
        return this;
        
    }
    
object SWITCHERCLIENT_OnSocketStatus_2 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 195;
        CONNECT_STATUS_FB  .Value = (ushort) ( __SocketInfo__.SocketStatus ) ; 
        __context__.SourceCodeLine = 196;
        if ( Functions.TestForTrue  ( ( CONNECT_STATUS_FB  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 198;
            SWITCHERDEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 199;
            CONNECT_FB  .Value = (ushort) ( 1 ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 203;
            SWITCHERDEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 204;
            SWITCHERDEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 205;
            CONNECT_FB  .Value = (ushort) ( 0 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object SWITCHERCLIENT_OnSocketReceive_3 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 211;
        SWITCHERDEVICE . RXQUEUE  .UpdateValue ( SWITCHERDEVICE . RXQUEUE + SWITCHERCLIENT .  SocketRxBuf  ) ; 
        __context__.SourceCodeLine = 212;
        Functions.ClearBuffer ( SWITCHERCLIENT .  SocketRxBuf ) ; 
        __context__.SourceCodeLine = 213;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( SWITCHERDEVICE.RXQUEUE ) > 1 ))  ) ) 
            {
            __context__.SourceCodeLine = 214;
            PARSEFEEDBACK (  __context__  ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object CONNECT_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 218;
        if ( Functions.TestForTrue  ( ( CONNECT  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 220;
            SWITCHERDEVICE . STATUSCONNECTREQUEST = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 221;
            CONNECTDISCONNECT (  __context__ , (ushort)( 1 )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 225;
            SWITCHERDEVICE . STATUSCONNECTREQUEST = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 226;
            CONNECTDISCONNECT (  __context__ , (ushort)( 0 )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_ADDRESS_OnChange_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 231;
        SWITCHERDEVICE . IPADDRESS  .UpdateValue ( IP_ADDRESS  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_PORT_OnChange_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 235;
        SWITCHERDEVICE . IPPORT = (ushort) ( IP_PORT  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object RX_OnChange_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 239;
        SWITCHERDEVICE . RXQUEUE  .UpdateValue ( SWITCHERDEVICE . RXQUEUE + RX  ) ; 
        __context__.SourceCodeLine = 240;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( SWITCHERDEVICE.RXQUEUE ) > 1 ))  ) ) 
            {
            __context__.SourceCodeLine = 241;
            PARSEFEEDBACK (  __context__  ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POLL_OnPush_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVCOUNTER = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object MANUALCMD_OnChange_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 258;
        SETQUEUE (  __context__ , MANUALCMD) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SELECT_DISPLAY_OnPush_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 262;
        GVDISPLAY = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ROUTE_SOURCE_OnPush_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 266;
        GVSOURCE = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 267;
        ROUTEDISPLAY (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SOURCEXTODISPLAY_OnChange_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 271;
        GVDISPLAY = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 272;
        GVSOURCE = (ushort) ( SOURCEXTODISPLAY[ GVDISPLAY ] .UshortValue ) ; 
        __context__.SourceCodeLine = 273;
        ROUTEDISPLAY (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 290;
        SWITCHERDEVICE . IPPORT = (ushort) ( 23 ) ; 
        __context__.SourceCodeLine = 291;
        SWITCHERDEVICE . IPADDRESS  .UpdateValue ( "192.168.10.254"  ) ; 
        __context__.SourceCodeLine = 292;
        SWITCHERDEVICE . ETX  .UpdateValue ( "\u000D\u000A"  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    SocketInfo __socketinfo__ = new SocketInfo( 1, this );
    InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
    _SplusNVRAM = new SplusNVRAM( this );
    SWITCHERCLIENT  = new SplusTcpClient ( 2047, this );
    SWITCHERDEVICE  = new SSWITCHER( this, true );
    SWITCHERDEVICE .PopulateCustomAttributeList( false );
    
    CONNECT = new Crestron.Logos.SplusObjects.DigitalInput( CONNECT__DigitalInput__, this );
    m_DigitalInputList.Add( CONNECT__DigitalInput__, CONNECT );
    
    POLL = new Crestron.Logos.SplusObjects.DigitalInput( POLL__DigitalInput__, this );
    m_DigitalInputList.Add( POLL__DigitalInput__, POLL );
    
    DEBUG = new Crestron.Logos.SplusObjects.DigitalInput( DEBUG__DigitalInput__, this );
    m_DigitalInputList.Add( DEBUG__DigitalInput__, DEBUG );
    
    SELECT_DISPLAY = new InOutArray<DigitalInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        SELECT_DISPLAY[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( SELECT_DISPLAY__DigitalInput__ + i, SELECT_DISPLAY__DigitalInput__, this );
        m_DigitalInputList.Add( SELECT_DISPLAY__DigitalInput__ + i, SELECT_DISPLAY[i+1] );
    }
    
    ROUTE_SOURCE = new InOutArray<DigitalInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        ROUTE_SOURCE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( ROUTE_SOURCE__DigitalInput__ + i, ROUTE_SOURCE__DigitalInput__, this );
        m_DigitalInputList.Add( ROUTE_SOURCE__DigitalInput__ + i, ROUTE_SOURCE[i+1] );
    }
    
    CONNECT_FB = new Crestron.Logos.SplusObjects.DigitalOutput( CONNECT_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( CONNECT_FB__DigitalOutput__, CONNECT_FB );
    
    IP_PORT = new Crestron.Logos.SplusObjects.AnalogInput( IP_PORT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( IP_PORT__AnalogSerialInput__, IP_PORT );
    
    SOURCEXTODISPLAY = new InOutArray<AnalogInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        SOURCEXTODISPLAY[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( SOURCEXTODISPLAY__AnalogSerialInput__ + i, SOURCEXTODISPLAY__AnalogSerialInput__, this );
        m_AnalogInputList.Add( SOURCEXTODISPLAY__AnalogSerialInput__ + i, SOURCEXTODISPLAY[i+1] );
    }
    
    CONNECT_STATUS_FB = new Crestron.Logos.SplusObjects.AnalogOutput( CONNECT_STATUS_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CONNECT_STATUS_FB__AnalogSerialOutput__, CONNECT_STATUS_FB );
    
    SOURCEXONDISPLAY = new InOutArray<AnalogOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        SOURCEXONDISPLAY[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( SOURCEXONDISPLAY__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( SOURCEXONDISPLAY__AnalogSerialOutput__ + i, SOURCEXONDISPLAY[i+1] );
    }
    
    IP_ADDRESS = new Crestron.Logos.SplusObjects.StringInput( IP_ADDRESS__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( IP_ADDRESS__AnalogSerialInput__, IP_ADDRESS );
    
    RX = new Crestron.Logos.SplusObjects.StringInput( RX__AnalogSerialInput__, 127, this );
    m_StringInputList.Add( RX__AnalogSerialInput__, RX );
    
    MANUALCMD = new Crestron.Logos.SplusObjects.StringInput( MANUALCMD__AnalogSerialInput__, 127, this );
    m_StringInputList.Add( MANUALCMD__AnalogSerialInput__, MANUALCMD );
    
    TX = new Crestron.Logos.SplusObjects.StringOutput( TX__AnalogSerialOutput__, this );
    m_StringOutputList.Add( TX__AnalogSerialOutput__, TX );
    
    
    SWITCHERCLIENT.OnSocketConnect.Add( new SocketHandlerWrapper( SWITCHERCLIENT_OnSocketConnect_0, false ) );
    SWITCHERCLIENT.OnSocketDisconnect.Add( new SocketHandlerWrapper( SWITCHERCLIENT_OnSocketDisconnect_1, false ) );
    SWITCHERCLIENT.OnSocketStatus.Add( new SocketHandlerWrapper( SWITCHERCLIENT_OnSocketStatus_2, false ) );
    SWITCHERCLIENT.OnSocketReceive.Add( new SocketHandlerWrapper( SWITCHERCLIENT_OnSocketReceive_3, false ) );
    CONNECT.OnDigitalChange.Add( new InputChangeHandlerWrapper( CONNECT_OnChange_4, false ) );
    IP_ADDRESS.OnSerialChange.Add( new InputChangeHandlerWrapper( IP_ADDRESS_OnChange_5, false ) );
    IP_PORT.OnAnalogChange.Add( new InputChangeHandlerWrapper( IP_PORT_OnChange_6, false ) );
    RX.OnSerialChange.Add( new InputChangeHandlerWrapper( RX_OnChange_7, false ) );
    POLL.OnDigitalPush.Add( new InputChangeHandlerWrapper( POLL_OnPush_8, false ) );
    MANUALCMD.OnSerialChange.Add( new InputChangeHandlerWrapper( MANUALCMD_OnChange_9, false ) );
    for( uint i = 0; i < 8; i++ )
        SELECT_DISPLAY[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( SELECT_DISPLAY_OnPush_10, false ) );
        
    for( uint i = 0; i < 8; i++ )
        ROUTE_SOURCE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( ROUTE_SOURCE_OnPush_11, false ) );
        
    for( uint i = 0; i < 8; i++ )
        SOURCEXTODISPLAY[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( SOURCEXTODISPLAY_OnChange_12, false ) );
        
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_LINK_MX88_4KUHDE_V0_3 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint CONNECT__DigitalInput__ = 0;
const uint POLL__DigitalInput__ = 1;
const uint DEBUG__DigitalInput__ = 2;
const uint IP_PORT__AnalogSerialInput__ = 0;
const uint IP_ADDRESS__AnalogSerialInput__ = 1;
const uint RX__AnalogSerialInput__ = 2;
const uint MANUALCMD__AnalogSerialInput__ = 3;
const uint SELECT_DISPLAY__DigitalInput__ = 3;
const uint ROUTE_SOURCE__DigitalInput__ = 11;
const uint SOURCEXTODISPLAY__AnalogSerialInput__ = 4;
const uint CONNECT_FB__DigitalOutput__ = 0;
const uint CONNECT_STATUS_FB__AnalogSerialOutput__ = 0;
const uint TX__AnalogSerialOutput__ = 1;
const uint SOURCEXONDISPLAY__AnalogSerialOutput__ = 2;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SSWITCHER : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  STATUSCONNECTREQUEST = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  STATUSCONNECTED = 0;
    
    [SplusStructAttribute(2, false, false)]
    public ushort  STATUSCOMMUNICATING = 0;
    
    [SplusStructAttribute(3, false, false)]
    public CrestronString  ETX;
    
    [SplusStructAttribute(4, false, false)]
    public CrestronString  IPADDRESS;
    
    [SplusStructAttribute(5, false, false)]
    public ushort  IPPORT = 0;
    
    [SplusStructAttribute(6, false, false)]
    public CrestronString  TXQUEUE;
    
    [SplusStructAttribute(7, false, false)]
    public CrestronString  RXQUEUE;
    
    
    public SSWITCHER( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        ETX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2, Owner );
        IPADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        TXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, Owner );
        RXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, Owner );
        
        
    }
    
}

}
