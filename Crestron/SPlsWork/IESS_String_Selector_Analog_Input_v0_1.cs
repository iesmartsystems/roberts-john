using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_STRING_SELECTOR_ANALOG_INPUT_V0_1
{
    public class UserModuleClass_IESS_STRING_SELECTOR_ANALOG_INPUT_V0_1 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> ACTIVESOURCE;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> SOURCENAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> OUTPUTSOURCESTRING;
        ushort GVSELECTSOURCE = 0;
        object ACTIVESOURCE_OnChange_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                ushort LVINPUT = 0;
                
                
                __context__.SourceCodeLine = 75;
                LVINPUT = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
                __context__.SourceCodeLine = 76;
                if ( Functions.TestForTrue  ( ( ACTIVESOURCE[ LVINPUT ] .UshortValue)  ) ) 
                    {
                    __context__.SourceCodeLine = 77;
                    OUTPUTSOURCESTRING [ LVINPUT]  .UpdateValue ( SOURCENAME [ ACTIVESOURCE[ LVINPUT ] .UshortValue ]  ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 79;
                    OUTPUTSOURCESTRING [ LVINPUT]  .UpdateValue ( ""  ) ; 
                    }
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    public override object FunctionMain (  object __obj__ ) 
        { 
        try
        {
            SplusExecutionContext __context__ = SplusFunctionMainStartCode();
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler(); }
        return __obj__;
        }
        
    
    public override void LogosSplusInitialize()
    {
        _SplusNVRAM = new SplusNVRAM( this );
        
        ACTIVESOURCE = new InOutArray<AnalogInput>( 16, this );
        for( uint i = 0; i < 16; i++ )
        {
            ACTIVESOURCE[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( ACTIVESOURCE__AnalogSerialInput__ + i, ACTIVESOURCE__AnalogSerialInput__, this );
            m_AnalogInputList.Add( ACTIVESOURCE__AnalogSerialInput__ + i, ACTIVESOURCE[i+1] );
        }
        
        SOURCENAME = new InOutArray<StringInput>( 16, this );
        for( uint i = 0; i < 16; i++ )
        {
            SOURCENAME[i+1] = new Crestron.Logos.SplusObjects.StringInput( SOURCENAME__AnalogSerialInput__ + i, SOURCENAME__AnalogSerialInput__, 31, this );
            m_StringInputList.Add( SOURCENAME__AnalogSerialInput__ + i, SOURCENAME[i+1] );
        }
        
        OUTPUTSOURCESTRING = new InOutArray<StringOutput>( 16, this );
        for( uint i = 0; i < 16; i++ )
        {
            OUTPUTSOURCESTRING[i+1] = new Crestron.Logos.SplusObjects.StringOutput( OUTPUTSOURCESTRING__AnalogSerialOutput__ + i, this );
            m_StringOutputList.Add( OUTPUTSOURCESTRING__AnalogSerialOutput__ + i, OUTPUTSOURCESTRING[i+1] );
        }
        
        
        for( uint i = 0; i < 16; i++ )
            ACTIVESOURCE[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( ACTIVESOURCE_OnChange_0, false ) );
            
        
        _SplusNVRAM.PopulateCustomAttributeList( true );
        
        NVRAM = _SplusNVRAM;
        
    }
    
    public override void LogosSimplSharpInitialize()
    {
        
        
    }
    
    public UserModuleClass_IESS_STRING_SELECTOR_ANALOG_INPUT_V0_1 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}
    
    
    
    
    const uint ACTIVESOURCE__AnalogSerialInput__ = 0;
    const uint SOURCENAME__AnalogSerialInput__ = 16;
    const uint OUTPUTSOURCESTRING__AnalogSerialOutput__ = 0;
    
    [SplusStructAttribute(-1, true, false)]
    public class SplusNVRAM : SplusStructureBase
    {
    
        public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
        
        
    }
    
    SplusNVRAM _SplusNVRAM = null;
    
    public class __CEvent__ : CEvent
    {
        public __CEvent__() {}
        public void Close() { base.Close(); }
        public int Reset() { return base.Reset() ? 1 : 0; }
        public int Set() { return base.Set() ? 1 : 0; }
        public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
    }
    public class __CMutex__ : CMutex
    {
        public __CMutex__() {}
        public void Close() { base.Close(); }
        public void ReleaseMutex() { base.ReleaseMutex(); }
        public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
    }
     public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
