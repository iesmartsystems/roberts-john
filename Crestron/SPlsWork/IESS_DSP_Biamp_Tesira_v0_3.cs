using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_DSP_BIAMP_TESIRA_V0_3
{
    public class UserModuleClass_IESS_DSP_BIAMP_TESIRA_V0_3 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput CONNECT;
        Crestron.Logos.SplusObjects.DigitalInput POLL;
        Crestron.Logos.SplusObjects.DigitalInput DEBUG;
        Crestron.Logos.SplusObjects.AnalogInput IP_PORT;
        Crestron.Logos.SplusObjects.StringInput IP_ADDRESS;
        Crestron.Logos.SplusObjects.StringInput LOGIN_NAME;
        Crestron.Logos.SplusObjects.StringInput LOGIN_PASSWORD;
        Crestron.Logos.SplusObjects.StringInput RX;
        Crestron.Logos.SplusObjects.StringInput MANUALCMD;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEUP;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEDOWN;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTETOGGLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTEON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTEOFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_0;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_1;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_2;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_3;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_4;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_5;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_6;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_7;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_8;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_9;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_STAR;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_POUND;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_BACKSPACE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_AUTOANSWERTOGGLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_AUTOANSWERON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_AUTOANSWEROFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DNDTOGGLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DNDON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DNDOFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DIAL;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_END;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_ACCEPT;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DECLINE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_JOIN;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_CONFERENCE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_REDIAL;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> VOLUMESET;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> VOLUMESTEP;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> GROUP;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> VOIP_CALLAPPEARANCE;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> INSTANCETAGS;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> VOIP_DIALENTRY;
        Crestron.Logos.SplusObjects.DigitalOutput CONNECT_FB;
        Crestron.Logos.SplusObjects.AnalogOutput CONNECT_STATUS_FB;
        Crestron.Logos.SplusObjects.StringOutput TX;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOLUMEMUTE_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_CALLSTATUS_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_CALLINCOMING_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_AUTOANSWER_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_DND_FB;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> VOLUMELEVEL_FB;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> GROUP_FB;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VOIP_DIALSTRING;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VOIP_CALLINCOMINGNAME_FB;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VOIP_CALLINCOMINGNUM;
        SplusTcpClient AUDIOCLIENT;
        UShortParameter UNIQUE_ID;
        SAUDIO AUDIODEVICE;
        ushort CONNECTION_STATUS = 0;
        ushort GVCOUNTER = 0;
        ushort GVVOIPCOUNTER = 0;
        ushort GVQUEUEPACER = 0;
        private void CONNECTDISCONNECT (  SplusExecutionContext __context__, ushort LVCONNECT ) 
            { 
            short LVSTATUS = 0;
            
            
            __context__.SourceCodeLine = 120;
            if ( Functions.TestForTrue  ( ( Functions.Not( LVCONNECT ))  ) ) 
                { 
                __context__.SourceCodeLine = 122;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 23))  ) ) 
                    { 
                    __context__.SourceCodeLine = 124;
                    if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSCONNECTED)  ) ) 
                        {
                        __context__.SourceCodeLine = 125;
                        LVSTATUS = (short) ( Functions.SocketDisconnectClient( AUDIOCLIENT ) ) ; 
                        }
                    
                    } 
                
                __context__.SourceCodeLine = 127;
                AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 128;
                AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 130;
                if ( Functions.TestForTrue  ( ( LVCONNECT)  ) ) 
                    { 
                    __context__.SourceCodeLine = 132;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 23))  ) ) 
                        { 
                        __context__.SourceCodeLine = 134;
                        if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSCONNECTED ))  ) ) 
                            {
                            __context__.SourceCodeLine = 135;
                            LVSTATUS = (short) ( Functions.SocketConnectClient( AUDIOCLIENT , AUDIODEVICE.IPADDRESS , (ushort)( AUDIODEVICE.IPPORT ) , (ushort)( 1 ) ) ) ; 
                            }
                        
                        } 
                    
                    } 
                
                }
            
            
            }
            
        private short VOLUMECONVERTER (  SplusExecutionContext __context__, short LVMINIMUM , short LVMAXIMUM , ushort LVVOLUMEINCOMING ) 
            { 
            ushort LVVOLUMERANGE = 0;
            ushort LVBARGRAPHMAX = 0;
            
            uint LVVOLUMEMULTIPLIER = 0;
            
            short LVVOLUMELEVEL = 0;
            short LVFMIN = 0;
            short LVFMAX = 0;
            
            
            __context__.SourceCodeLine = 146;
            LVBARGRAPHMAX = (ushort) ( 65535 ) ; 
            __context__.SourceCodeLine = 148;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 150;
                LVFMIN = (short) ( (LVMINIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                __context__.SourceCodeLine = 151;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM < 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 153;
                    LVFMAX = (short) ( (LVMAXIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                    __context__.SourceCodeLine = 154;
                    LVVOLUMERANGE = (ushort) ( (LVFMIN - LVFMAX) ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 156;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM >= 0 ))  ) ) 
                        {
                        __context__.SourceCodeLine = 157;
                        LVVOLUMERANGE = (ushort) ( (LVFMIN + LVMAXIMUM) ) ; 
                        }
                    
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 160;
                LVVOLUMERANGE = (ushort) ( (LVMINIMUM + LVMAXIMUM) ) ; 
                }
            
            __context__.SourceCodeLine = 162;
            LVVOLUMEMULTIPLIER = (uint) ( ((LVVOLUMEINCOMING * 100) / LVBARGRAPHMAX) ) ; 
            __context__.SourceCodeLine = 163;
            LVVOLUMEMULTIPLIER = (uint) ( (LVVOLUMEMULTIPLIER * LVVOLUMERANGE) ) ; 
            __context__.SourceCodeLine = 164;
            LVVOLUMELEVEL = (short) ( (LVVOLUMEMULTIPLIER / 100) ) ; 
            __context__.SourceCodeLine = 166;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 167;
                LVVOLUMELEVEL = (short) ( (LVVOLUMELEVEL + LVMINIMUM) ) ; 
                }
            
            __context__.SourceCodeLine = 169;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL < LVMINIMUM ))  ) ) 
                {
                __context__.SourceCodeLine = 170;
                LVVOLUMELEVEL = (short) ( LVMINIMUM ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 171;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL > LVMAXIMUM ))  ) ) 
                    {
                    __context__.SourceCodeLine = 172;
                    LVVOLUMELEVEL = (short) ( LVMAXIMUM ) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 174;
            return (short)( LVVOLUMELEVEL) ; 
            
            }
            
        private uint VOLUMECONVERTERREVERSE (  SplusExecutionContext __context__, short LVMINIMUM , short LVMAXIMUM , short LVVOLUMEINCOMING ) 
            { 
            ushort LVVOLUMERANGE = 0;
            ushort LVBARGRAPHMAX = 0;
            ushort LVINC = 0;
            ushort LVMULT = 0;
            
            uint LVVOLUMELEVEL = 0;
            
            short LVFMIN = 0;
            short LVFMAX = 0;
            
            
            __context__.SourceCodeLine = 182;
            LVBARGRAPHMAX = (ushort) ( 65535 ) ; 
            __context__.SourceCodeLine = 184;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 186;
                LVFMIN = (short) ( (LVMINIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                __context__.SourceCodeLine = 187;
                LVINC = (ushort) ( (LVVOLUMEINCOMING + LVFMIN) ) ; 
                __context__.SourceCodeLine = 188;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM < 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 190;
                    LVFMAX = (short) ( (LVMAXIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                    __context__.SourceCodeLine = 191;
                    LVVOLUMERANGE = (ushort) ( (LVFMIN - LVFMAX) ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 193;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM >= 0 ))  ) ) 
                        {
                        __context__.SourceCodeLine = 194;
                        LVVOLUMERANGE = (ushort) ( (LVFMIN + LVMAXIMUM) ) ; 
                        }
                    
                    }
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 198;
                LVVOLUMERANGE = (ushort) ( (LVMINIMUM + LVMAXIMUM) ) ; 
                __context__.SourceCodeLine = 199;
                LVINC = (ushort) ( LVVOLUMEINCOMING ) ; 
                } 
            
            __context__.SourceCodeLine = 202;
            LVMULT = (ushort) ( (LVBARGRAPHMAX / LVVOLUMERANGE) ) ; 
            __context__.SourceCodeLine = 203;
            LVVOLUMELEVEL = (uint) ( (LVINC * LVMULT) ) ; 
            __context__.SourceCodeLine = 205;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL < 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 206;
                LVVOLUMELEVEL = (uint) ( 0 ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 207;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL > LVBARGRAPHMAX ))  ) ) 
                    {
                    __context__.SourceCodeLine = 208;
                    LVVOLUMELEVEL = (uint) ( LVBARGRAPHMAX ) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 209;
            return (uint)( LVVOLUMELEVEL) ; 
            
            }
            
        private void SETDEBUG (  SplusExecutionContext __context__, CrestronString LVSTRING , ushort LVTYPE ) 
            { 
            
            __context__.SourceCodeLine = 213;
            if ( Functions.TestForTrue  ( ( DEBUG  .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 215;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVTYPE == 1))  ) ) 
                    {
                    __context__.SourceCodeLine = 216;
                    Trace( "Biamp RX: {0}", LVSTRING ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 218;
                    Trace( "Biamp TX: {0}", LVSTRING ) ; 
                    }
                
                } 
            
            
            }
            
        private void SENDTX (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 223;
            while ( Functions.TestForTrue  ( ( Functions.Find( "\u000B\u000B" , AUDIODEVICE.TXQUEUE ))  ) ) 
                { 
                __context__.SourceCodeLine = 225;
                AUDIODEVICE . TXQUEUEING = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 226;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_16__" , GVQUEUEPACER , __SPLS_TMPVAR__WAITLABEL_16___Callback ) ;
                __context__.SourceCodeLine = 223;
                } 
            
            __context__.SourceCodeLine = 253;
            AUDIODEVICE . TXQUEUEING = (ushort) ( 0 ) ; 
            
            }
            
        public void __SPLS_TMPVAR__WAITLABEL_16___CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            
            CrestronString LVTEMP;
            CrestronString LVTRASH;
            LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 511, this );
            LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
            
            ushort LVINDEX = 0;
            ushort LVINDEX2ND = 0;
            
            __context__.SourceCodeLine = 230;
            LVTEMP  .UpdateValue ( Functions.Remove ( "\u000B\u000B" , AUDIODEVICE . TXQUEUE )  ) ; 
            __context__.SourceCodeLine = 231;
            LVTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTEMP ) - 2), LVTEMP )  ) ; 
            __context__.SourceCodeLine = 232;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( LVTEMP ) > 1 ))  ) ) 
                { 
                __context__.SourceCodeLine = 234;
                if ( Functions.TestForTrue  ( ( Functions.Find( "INIT:START" , LVTEMP ))  ) ) 
                    {
                    __context__.SourceCodeLine = 235;
                    GVQUEUEPACER = (ushort) ( 20 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 236;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "INIT:END" , LVTEMP ))  ) ) 
                        {
                        __context__.SourceCodeLine = 237;
                        GVQUEUEPACER = (ushort) ( 0 ) ; 
                        }
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 240;
                        LVTEMP  .UpdateValue ( LVTEMP + AUDIODEVICE . ETX  ) ; 
                        __context__.SourceCodeLine = 241;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 23))  ) ) 
                            { 
                            __context__.SourceCodeLine = 243;
                            if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSCONNECTED)  ) ) 
                                {
                                __context__.SourceCodeLine = 244;
                                Functions.SocketSend ( AUDIOCLIENT , LVTEMP ) ; 
                                }
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 247;
                            TX  .UpdateValue ( LVTEMP  ) ; 
                            }
                        
                        __context__.SourceCodeLine = 248;
                        SETDEBUG (  __context__ , LVTEMP, (ushort)( 0 )) ; 
                        } 
                    
                    }
                
                } 
            
            
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    private void SETQUEUE (  SplusExecutionContext __context__, CrestronString LVSTRING ) 
        { 
        CrestronString LVTEMP;
        LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 4095, this );
        
        
        __context__.SourceCodeLine = 262;
        LVTEMP  .UpdateValue ( LVSTRING + AUDIODEVICE . ETX  ) ; 
        __context__.SourceCodeLine = 263;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 23))  ) ) 
            { 
            __context__.SourceCodeLine = 265;
            if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSCONNECTED)  ) ) 
                {
                __context__.SourceCodeLine = 266;
                Functions.SocketSend ( AUDIOCLIENT , LVTEMP ) ; 
                }
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 269;
            TX  .UpdateValue ( LVTEMP  ) ; 
            }
        
        __context__.SourceCodeLine = 270;
        SETDEBUG (  __context__ , LVTEMP, (ushort)( 0 )) ; 
        
        }
        
    private void VOIPUPDATEAPPEARANCE (  SplusExecutionContext __context__, ushort LVINDEX ) 
        { 
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
        
        
        __context__.SourceCodeLine = 275;
        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " callAppearance " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.STATUSVOIPAPPEARANCE[ LVINDEX ] ) )  ) ; 
        __context__.SourceCodeLine = 276;
        SETQUEUE (  __context__ , LVSTRING) ; 
        
        }
        
    private void VOIP_NUM (  SplusExecutionContext __context__, ushort LVINDEX , CrestronString LVNUM ) 
        { 
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 281;
        if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSVOIPCALLSTATEAPPEARANCE[ LVINDEX , AUDIODEVICE.STATUSVOIPAPPEARANCE[ LVINDEX ] ] ))  ) ) 
            {
            __context__.SourceCodeLine = 282;
            AUDIODEVICE . VOIPDIALSTRING [ LVINDEX ]  .UpdateValue ( AUDIODEVICE . VOIPDIALSTRING [ LVINDEX ] + LVNUM  ) ; 
            }
        
        else 
            { 
            __context__.SourceCodeLine = 285;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " dtmf " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVNUM  ) ; 
            __context__.SourceCodeLine = 286;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        
        }
        
    private void POLLINSTANCE (  SplusExecutionContext __context__, ushort LVINDEX ) 
        { 
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 292;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.INSTANCETAGSPOLL[ LVINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 294;
            if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 296;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " get crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) )  ) ; 
                __context__.SourceCodeLine = 297;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 299;
                if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 301;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " get crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ LVINDEX ] ) )  ) ; 
                    __context__.SourceCodeLine = 302;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 304;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 306;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " get input " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) )  ) ; 
                        __context__.SourceCodeLine = 307;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 309;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 311;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " get muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) )  ) ; 
                            __context__.SourceCodeLine = 312;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            __context__.SourceCodeLine = 313;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " get group " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) )  ) ; 
                            __context__.SourceCodeLine = 314;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 316;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 318;
                                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " get state " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) )  ) ; 
                                __context__.SourceCodeLine = 319;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            } 
        
        
        }
        
    private void INITIALIZEINSTANCE (  SplusExecutionContext __context__, ushort LVINDEX ) 
        { 
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 326;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.INSTANCETAGSNAME[ LVINDEX ] ) > 2 ))  ) ) 
            { 
            __context__.SourceCodeLine = 328;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 330;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe level " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ]  ) ; 
                __context__.SourceCodeLine = 331;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 332;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe mute " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ]  ) ; 
                __context__.SourceCodeLine = 333;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 334;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " get minLevel " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) )  ) ; 
                __context__.SourceCodeLine = 335;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 336;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " get maxLevel " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) )  ) ; 
                __context__.SourceCodeLine = 337;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 338;
                AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 340;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 342;
                    AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 1 ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 344;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 346;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe sourceSelection " + INSTANCETAGS [ LVINDEX ]  ) ; 
                        __context__.SourceCodeLine = 347;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        __context__.SourceCodeLine = 348;
                        AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 350;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 352;
                            AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 1 ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 354;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 356;
                                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe levelOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ]  ) ; 
                                __context__.SourceCodeLine = 357;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                __context__.SourceCodeLine = 358;
                                AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 359;
                                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " get levelOutMin " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) )  ) ; 
                                __context__.SourceCodeLine = 360;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                __context__.SourceCodeLine = 361;
                                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " get levelOutMax " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) )  ) ; 
                                __context__.SourceCodeLine = 362;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                __context__.SourceCodeLine = 363;
                                AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 1 ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 365;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 367;
                                    AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 1 ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 369;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "VOIP" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 371;
                                        GVVOIPCOUNTER = (ushort) ( (GVVOIPCOUNTER + 1) ) ; 
                                        __context__.SourceCodeLine = 372;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GVVOIPCOUNTER <= 2 ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 373;
                                            AUDIODEVICE . VOIPINSTANCEINDEX [ GVVOIPCOUNTER] = (ushort) ( LVINDEX ) ; 
                                            }
                                        
                                        __context__.SourceCodeLine = 374;
                                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe autoAnswer " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-autoAnswer"  ) ; 
                                        __context__.SourceCodeLine = 375;
                                        SETQUEUE (  __context__ , LVSTRING) ; 
                                        __context__.SourceCodeLine = 376;
                                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe callState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-callState"  ) ; 
                                        __context__.SourceCodeLine = 377;
                                        SETQUEUE (  __context__ , LVSTRING) ; 
                                        __context__.SourceCodeLine = 378;
                                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe dndEnable " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-dndEnable"  ) ; 
                                        __context__.SourceCodeLine = 379;
                                        SETQUEUE (  __context__ , LVSTRING) ; 
                                        __context__.SourceCodeLine = 380;
                                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe ringing " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-ringing"  ) ; 
                                        __context__.SourceCodeLine = 381;
                                        SETQUEUE (  __context__ , LVSTRING) ; 
                                        __context__.SourceCodeLine = 382;
                                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe cidUser " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-cidUser"  ) ; 
                                        __context__.SourceCodeLine = 383;
                                        SETQUEUE (  __context__ , LVSTRING) ; 
                                        __context__.SourceCodeLine = 384;
                                        VOIPUPDATEAPPEARANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 386;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "POTS" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 388;
                                            GVVOIPCOUNTER = (ushort) ( (GVVOIPCOUNTER + 1) ) ; 
                                            __context__.SourceCodeLine = 389;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GVVOIPCOUNTER <= 2 ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 390;
                                                AUDIODEVICE . VOIPINSTANCEINDEX [ GVVOIPCOUNTER] = (ushort) ( LVINDEX ) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 391;
                                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe autoAnswer " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-autoAnswer"  ) ; 
                                            __context__.SourceCodeLine = 392;
                                            SETQUEUE (  __context__ , LVSTRING) ; 
                                            __context__.SourceCodeLine = 393;
                                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe callState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-callState"  ) ; 
                                            __context__.SourceCodeLine = 394;
                                            SETQUEUE (  __context__ , LVSTRING) ; 
                                            __context__.SourceCodeLine = 395;
                                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe dndEnable " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-dndEnable"  ) ; 
                                            __context__.SourceCodeLine = 396;
                                            SETQUEUE (  __context__ , LVSTRING) ; 
                                            __context__.SourceCodeLine = 397;
                                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe ringing " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-ringing"  ) ; 
                                            __context__.SourceCodeLine = 398;
                                            SETQUEUE (  __context__ , LVSTRING) ; 
                                            __context__.SourceCodeLine = 399;
                                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe cidUser " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-cidUser"  ) ; 
                                            __context__.SourceCodeLine = 400;
                                            SETQUEUE (  __context__ , LVSTRING) ; 
                                            } 
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            } 
        
        
        }
        
    private void DEVICEONLINE (  SplusExecutionContext __context__ ) 
        { 
        ushort LVCOUNTER = 0;
        
        
        __context__.SourceCodeLine = 408;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)64; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 410;
            INITIALIZEINSTANCE (  __context__ , (ushort)( LVCOUNTER )) ; 
            __context__.SourceCodeLine = 408;
            } 
        
        __context__.SourceCodeLine = 412;
        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_17__" , 1000 , __SPLS_TMPVAR__WAITLABEL_17___Callback ) ;
        
        }
        
    public void __SPLS_TMPVAR__WAITLABEL_17___CallbackFn( object stateInfo )
    {
    
        try
        {
            Wait __LocalWait__ = (Wait)stateInfo;
            SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
            __LocalWait__.RemoveFromList();
            
            
            ushort LVCOUNTER2 = 0;
            
            __context__.SourceCodeLine = 415;
            ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__2 = (ushort)64; 
            int __FN_FORSTEP_VAL__2 = (int)1; 
            for ( LVCOUNTER2  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER2  >= __FN_FORSTART_VAL__2) && (LVCOUNTER2  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER2  <= __FN_FORSTART_VAL__2) && (LVCOUNTER2  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER2  += (ushort)__FN_FORSTEP_VAL__2) 
                { 
                __context__.SourceCodeLine = 417;
                POLLINSTANCE (  __context__ , (ushort)( LVCOUNTER2 )) ; 
                __context__.SourceCodeLine = 415;
                } 
            
            
        
        
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler(); }
        
    }
    
private void PARSEFEEDBACK (  SplusExecutionContext __context__ ) 
    { 
    ushort LVINDEX = 0;
    ushort LVINDEX2ND = 0;
    ushort LVCOUNTER = 0;
    ushort LVVOIPNUM = 0;
    ushort LVVOIPCOUNTER = 0;
    
    short LVVOL = 0;
    
    CrestronString LVRX;
    CrestronString LVTRASH;
    CrestronString LVTEMP;
    CrestronString LVSTRING;
    CrestronString LVTAG;
    LVRX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 65534, this );
    LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
    LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
    LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
    LVTAG  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
    
    
    __context__.SourceCodeLine = 427;
    while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D\u000A" , AUDIODEVICE.RXQUEUE ))  ) ) 
        { 
        __context__.SourceCodeLine = 429;
        AUDIODEVICE . RXQUEUEING = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 430;
        LVRX  .UpdateValue ( Functions.Remove ( "\u000D\u000A" , AUDIODEVICE . RXQUEUE )  ) ; 
        __context__.SourceCodeLine = 431;
        LVRX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVRX ) - 2), LVRX )  ) ; 
        __context__.SourceCodeLine = 432;
        SETDEBUG (  __context__ , LVRX, (ushort)( 1 )) ; 
        __context__.SourceCodeLine = 433;
        if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSCOMMUNICATING ))  ) ) 
            {
            __context__.SourceCodeLine = 434;
            AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 1 ) ; 
            }
        
        __context__.SourceCodeLine = 435;
        if ( Functions.TestForTrue  ( ( Functions.Find( "publishToken\u0022:\u0022" , LVRX ))  ) ) 
            { 
            __context__.SourceCodeLine = 437;
            LVTRASH  .UpdateValue ( Functions.Remove ( "publishToken\u0022:\u0022" , LVRX )  ) ; 
            __context__.SourceCodeLine = 438;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)64; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 440;
                if ( Functions.TestForTrue  ( ( Functions.Find( INSTANCETAGS[ LVCOUNTER ] , LVRX ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 442;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "value\u0022:" , LVRX ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 444;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSTYPE [ LVCOUNTER ]  ) ; 
                        __context__.SourceCodeLine = 445;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "SVOL") ) || Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "MVOL") )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "RCMB") )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "SSEL") )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 447;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "value\u0022:" , LVRX )  ) ; 
                            __context__.SourceCodeLine = 448;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "true" , LVRX ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 450;
                                AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 451;
                                VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 453;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "false" , LVRX ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 455;
                                    AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                    __context__.SourceCodeLine = 456;
                                    VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                    } 
                                
                                else 
                                    { 
                                    __context__.SourceCodeLine = 460;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVRX ))  ) ) 
                                        {
                                        __context__.SourceCodeLine = 461;
                                        LVVOL = (short) ( (Functions.Atoi( LVRX ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                        }
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 463;
                                        LVVOL = (short) ( Functions.Atoi( LVRX ) ) ; 
                                        }
                                    
                                    __context__.SourceCodeLine = 464;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == "SSEL"))  ) ) 
                                        {
                                        __context__.SourceCodeLine = 465;
                                        GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( LVVOL ) ; 
                                        }
                                    
                                    else 
                                        { 
                                        __context__.SourceCodeLine = 468;
                                        AUDIODEVICE . STATUSVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                        __context__.SourceCodeLine = 469;
                                        VOLUMELEVEL_FB [ LVCOUNTER]  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( AUDIODEVICE.VOLUMEMIN[ LVCOUNTER ] ) , (short)( AUDIODEVICE.VOLUMEMAX[ LVCOUNTER ] ) , (short)( LVVOL ) ) ) ; 
                                        __context__.SourceCodeLine = 470;
                                        if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.VOLUMEINUSE ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 471;
                                            AUDIODEVICE . INTERNALVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                            }
                                        
                                        } 
                                    
                                    } 
                                
                                }
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 475;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "VOIP") ) || Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "POTS") )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 477;
                                ushort __FN_FORSTART_VAL__2 = (ushort) ( 0 ) ;
                                ushort __FN_FOREND_VAL__2 = (ushort)2; 
                                int __FN_FORSTEP_VAL__2 = (int)1; 
                                for ( LVVOIPCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVVOIPCOUNTER  >= __FN_FORSTART_VAL__2) && (LVVOIPCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVVOIPCOUNTER  <= __FN_FORSTART_VAL__2) && (LVVOIPCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVVOIPCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                                    { 
                                    __context__.SourceCodeLine = 479;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVCOUNTER == AUDIODEVICE.VOIPINSTANCEINDEX[ LVVOIPCOUNTER ]))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 481;
                                        LVVOIPNUM = (ushort) ( LVVOIPCOUNTER ) ; 
                                        __context__.SourceCodeLine = 482;
                                        break ; 
                                        } 
                                    
                                    __context__.SourceCodeLine = 477;
                                    } 
                                
                                __context__.SourceCodeLine = 485;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( LVVOIPNUM > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVVOIPNUM <= 2 ) )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 487;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "true" , LVRX ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 489;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "autoAnswer" , LVRX ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 491;
                                            AUDIODEVICE . STATUSVOIPAUTOANSWER [ LVVOIPNUM] = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 492;
                                            VOIP_AUTOANSWER_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 494;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "dndEnable" , LVRX ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 496;
                                                AUDIODEVICE . STATUSVOIPDND [ LVVOIPNUM] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 497;
                                                VOIP_DND_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 499;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "ringing" , LVRX ))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 500;
                                                    VOIP_CALLINCOMING_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 502;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "false" , LVRX ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 504;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "autoAnswer" , LVRX ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 506;
                                                AUDIODEVICE . STATUSVOIPAUTOANSWER [ LVVOIPNUM] = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 507;
                                                VOIP_AUTOANSWER_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 509;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "dndEnable" , LVRX ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 511;
                                                    AUDIODEVICE . STATUSVOIPDND [ LVVOIPNUM] = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 512;
                                                    VOIP_DND_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 514;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "ringing" , LVRX ))  ) ) 
                                                        {
                                                        __context__.SourceCodeLine = 515;
                                                        VOIP_CALLINCOMING_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            } 
                                        
                                        else 
                                            { 
                                            __context__.SourceCodeLine = 519;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "cidUser" , LVRX ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 520;
                                                LVTEMP  .UpdateValue ( "cidUser"  ) ; 
                                                }
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 521;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "callState" , LVRX ))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 522;
                                                    LVTEMP  .UpdateValue ( "callState"  ) ; 
                                                    }
                                                
                                                }
                                            
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                } 
                            
                            }
                        
                        } 
                    
                    } 
                
                __context__.SourceCodeLine = 438;
                } 
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 530;
            if ( Functions.TestForTrue  ( ( Functions.Find( "get " , LVRX ))  ) ) 
                { 
                __context__.SourceCodeLine = 532;
                if ( Functions.TestForTrue  ( ( Functions.Find( "get " , LVRX ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 534;
                    LVTAG  .UpdateValue ( Functions.Remove ( " " , LVRX )  ) ; 
                    __context__.SourceCodeLine = 535;
                    LVTAG  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTAG ) - 1), LVTAG )  ) ; 
                    __context__.SourceCodeLine = 536;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "get " , LVRX )  ) ; 
                    __context__.SourceCodeLine = 537;
                    LVTEMP  .UpdateValue ( Functions.Remove ( " " , LVRX )  ) ; 
                    __context__.SourceCodeLine = 538;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "muteOut" , LVTEMP ))  ) ) 
                        {
                        __context__.SourceCodeLine = 539;
                        AUDIODEVICE . LASTPOLLSECONDINDEX = (ushort) ( 1 ) ; 
                        }
                    
                    __context__.SourceCodeLine = 540;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "group" , LVTEMP ))  ) ) 
                        {
                        __context__.SourceCodeLine = 541;
                        AUDIODEVICE . LASTPOLLSECONDINDEX = (ushort) ( 2 ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 542;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "minLevel" , LVTEMP ))  ) ) 
                            {
                            __context__.SourceCodeLine = 543;
                            AUDIODEVICE . LASTPOLLSECONDINDEX = (ushort) ( 3 ) ; 
                            }
                        
                        else 
                            {
                            __context__.SourceCodeLine = 544;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "maxLevel" , LVTEMP ))  ) ) 
                                {
                                __context__.SourceCodeLine = 545;
                                AUDIODEVICE . LASTPOLLSECONDINDEX = (ushort) ( 4 ) ; 
                                }
                            
                            }
                        
                        }
                    
                    __context__.SourceCodeLine = 546;
                    if ( Functions.TestForTrue  ( ( Functions.Find( " " , LVRX ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 548;
                        LVTEMP  .UpdateValue ( Functions.Remove ( " " , LVRX )  ) ; 
                        __context__.SourceCodeLine = 549;
                        LVTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTEMP ) - 1), LVTEMP )  ) ; 
                        __context__.SourceCodeLine = 550;
                        LVINDEX = (ushort) ( Functions.Atoi( LVTEMP ) ) ; 
                        __context__.SourceCodeLine = 551;
                        LVINDEX2ND = (ushort) ( Functions.Atoi( LVRX ) ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 554;
                        LVINDEX = (ushort) ( Functions.Atoi( LVRX ) ) ; 
                        }
                    
                    __context__.SourceCodeLine = 555;
                    ushort __FN_FORSTART_VAL__3 = (ushort) ( 1 ) ;
                    ushort __FN_FOREND_VAL__3 = (ushort)64; 
                    int __FN_FORSTEP_VAL__3 = (int)1; 
                    for ( LVCOUNTER  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__3) && (LVCOUNTER  <= __FN_FOREND_VAL__3) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__3) && (LVCOUNTER  >= __FN_FOREND_VAL__3) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__3) 
                        { 
                        __context__.SourceCodeLine = 557;
                        if ( Functions.TestForTrue  ( ( Functions.Find( AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ] , LVTAG ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 559;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] == LVINDEX))  ) ) 
                                { 
                                __context__.SourceCodeLine = 561;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 563;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INSTANCETAGSINDEXSECOND[ LVCOUNTER ] == LVINDEX2ND))  ) ) 
                                        {
                                        __context__.SourceCodeLine = 564;
                                        AUDIODEVICE . LASTPOLLINDEX = (ushort) ( LVCOUNTER ) ; 
                                        }
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 567;
                                    AUDIODEVICE . LASTPOLLINDEX = (ushort) ( LVCOUNTER ) ; 
                                    }
                                
                                } 
                            
                            } 
                        
                        __context__.SourceCodeLine = 555;
                        } 
                    
                    } 
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 573;
                if ( Functions.TestForTrue  ( ( Functions.Find( "+OK " , LVRX ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 575;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "+OK " , LVRX )  ) ; 
                    __context__.SourceCodeLine = 576;
                    if ( Functions.TestForTrue  ( ( AUDIODEVICE.LASTPOLLINDEX)  ) ) 
                        { 
                        __context__.SourceCodeLine = 578;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "value\u0022:" , LVRX ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 580;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "value\u0022:" , LVRX )  ) ; 
                            __context__.SourceCodeLine = 581;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSTYPE [ AUDIODEVICE.LASTPOLLINDEX ]  ) ; 
                            __context__.SourceCodeLine = 582;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "SMAT") ) || Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "MMAT") )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "LGCS") )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 584;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "true" , LVRX ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 586;
                                    AUDIODEVICE . STATUSVOLUMEMUTE [ AUDIODEVICE.LASTPOLLINDEX] = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 587;
                                    VOLUMEMUTE_FB [ AUDIODEVICE.LASTPOLLINDEX]  .Value = (ushort) ( 1 ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 589;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "false" , LVRX ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 591;
                                        AUDIODEVICE . STATUSVOLUMEMUTE [ AUDIODEVICE.LASTPOLLINDEX] = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 592;
                                        VOLUMEMUTE_FB [ AUDIODEVICE.LASTPOLLINDEX]  .Value = (ushort) ( 0 ) ; 
                                        } 
                                    
                                    }
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 595;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "ROUT") ) || Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "SSEL") )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 597;
                                    GROUP_FB [ AUDIODEVICE.LASTPOLLINDEX]  .Value = (ushort) ( Functions.Atoi( LVRX ) ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 599;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == "RCMB"))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 601;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "true" , LVRX ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 603;
                                            AUDIODEVICE . STATUSVOLUMEMUTE [ AUDIODEVICE.LASTPOLLINDEX] = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 604;
                                            VOLUMEMUTE_FB [ AUDIODEVICE.LASTPOLLINDEX]  .Value = (ushort) ( 1 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 606;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "false" , LVRX ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 608;
                                                AUDIODEVICE . STATUSVOLUMEMUTE [ AUDIODEVICE.LASTPOLLINDEX] = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 609;
                                                VOLUMEMUTE_FB [ AUDIODEVICE.LASTPOLLINDEX]  .Value = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            else 
                                                { 
                                                __context__.SourceCodeLine = 613;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.LASTPOLLSECONDINDEX == 2))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 614;
                                                    GROUP_FB [ AUDIODEVICE.LASTPOLLINDEX]  .Value = (ushort) ( Functions.Atoi( LVRX ) ) ; 
                                                    }
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 615;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.LASTPOLLSECONDINDEX == 3))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 617;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVRX ))  ) ) 
                                                            {
                                                            __context__.SourceCodeLine = 618;
                                                            LVVOL = (short) ( (Functions.Atoi( LVRX ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                                            }
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 620;
                                                            LVVOL = (short) ( Functions.Atoi( LVRX ) ) ; 
                                                            }
                                                        
                                                        __context__.SourceCodeLine = 621;
                                                        AUDIODEVICE . VOLUMEMIN [ AUDIODEVICE.LASTPOLLINDEX] = (short) ( LVVOL ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 623;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.LASTPOLLSECONDINDEX == 4))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 625;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVRX ))  ) ) 
                                                                {
                                                                __context__.SourceCodeLine = 626;
                                                                LVVOL = (short) ( (Functions.Atoi( LVRX ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                                                }
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 628;
                                                                LVVOL = (short) ( Functions.Atoi( LVRX ) ) ; 
                                                                }
                                                            
                                                            __context__.SourceCodeLine = 629;
                                                            AUDIODEVICE . VOLUMEMAX [ AUDIODEVICE.LASTPOLLINDEX] = (short) ( LVVOL ) ; 
                                                            } 
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                } 
                                            
                                            }
                                        
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 633;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "SVOL") ) || Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "MVOL") )) ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 635;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.LASTPOLLSECONDINDEX == 3))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 637;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVRX ))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 638;
                                                    LVVOL = (short) ( (Functions.Atoi( LVRX ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                                    }
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 640;
                                                    LVVOL = (short) ( Functions.Atoi( LVRX ) ) ; 
                                                    }
                                                
                                                __context__.SourceCodeLine = 641;
                                                AUDIODEVICE . VOLUMEMIN [ AUDIODEVICE.LASTPOLLINDEX] = (short) ( LVVOL ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 643;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.LASTPOLLSECONDINDEX == 4))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 645;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVRX ))  ) ) 
                                                        {
                                                        __context__.SourceCodeLine = 646;
                                                        LVVOL = (short) ( (Functions.Atoi( LVRX ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                                        }
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 648;
                                                        LVVOL = (short) ( Functions.Atoi( LVRX ) ) ; 
                                                        }
                                                    
                                                    __context__.SourceCodeLine = 649;
                                                    AUDIODEVICE . VOLUMEMAX [ AUDIODEVICE.LASTPOLLINDEX] = (short) ( LVVOL ) ; 
                                                    } 
                                                
                                                }
                                            
                                            } 
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 654;
                    AUDIODEVICE . LASTPOLLINDEX = (ushort) ( 0 ) ; 
                    } 
                
                }
            
            }
        
        __context__.SourceCodeLine = 427;
        } 
    
    __context__.SourceCodeLine = 657;
    AUDIODEVICE . RXQUEUEING = (ushort) ( 0 ) ; 
    
    }
    
object AUDIOCLIENT_OnSocketConnect_0 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
        
        
        __context__.SourceCodeLine = 666;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 23))  ) ) 
            { 
            __context__.SourceCodeLine = 668;
            AUDIODEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 669;
            CONNECT_FB  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 670;
            CONNECT_STATUS_FB  .Value = (ushort) ( AUDIOCLIENT.SocketStatus ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object AUDIOCLIENT_OnSocketDisconnect_1 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 676;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 23))  ) ) 
            { 
            __context__.SourceCodeLine = 678;
            AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 679;
            AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 680;
            CONNECT_FB  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 681;
            CONNECT_STATUS_FB  .Value = (ushort) ( AUDIOCLIENT.SocketStatus ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object AUDIOCLIENT_OnSocketStatus_2 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 686;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 23))  ) ) 
            { 
            __context__.SourceCodeLine = 688;
            CONNECT_STATUS_FB  .Value = (ushort) ( __SocketInfo__.SocketStatus ) ; 
            __context__.SourceCodeLine = 689;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CONNECT_STATUS_FB  .Value == 2))  ) ) 
                { 
                __context__.SourceCodeLine = 691;
                AUDIODEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 692;
                CONNECT_FB  .Value = (ushort) ( 1 ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 696;
                AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 697;
                AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 698;
                CONNECT_FB  .Value = (ushort) ( 0 ) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object AUDIOCLIENT_OnSocketReceive_3 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 705;
        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00FF\u00FD\u0018" , AUDIOCLIENT.SocketRxBuf ))  ) ) 
            {
            __context__.SourceCodeLine = 706;
            Functions.SocketSend ( AUDIOCLIENT , "\u00FF\u00FC\u0018" ) ; 
            }
        
        __context__.SourceCodeLine = 707;
        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00FF\u00FD\u0020" , AUDIOCLIENT.SocketRxBuf ))  ) ) 
            {
            __context__.SourceCodeLine = 708;
            Functions.SocketSend ( AUDIOCLIENT , "\u00FF\u00FC\u0020" ) ; 
            }
        
        __context__.SourceCodeLine = 709;
        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00FF\u00FD\u0023" , AUDIOCLIENT.SocketRxBuf ))  ) ) 
            {
            __context__.SourceCodeLine = 710;
            Functions.SocketSend ( AUDIOCLIENT , "\u00FF\u00FC\u0023" ) ; 
            }
        
        __context__.SourceCodeLine = 711;
        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00FF\u00FD\u0027" , AUDIOCLIENT.SocketRxBuf ))  ) ) 
            {
            __context__.SourceCodeLine = 712;
            Functions.SocketSend ( AUDIOCLIENT , "\u00FF\u00FC\u0027" ) ; 
            }
        
        __context__.SourceCodeLine = 713;
        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00FF\u00FD\u0024" , AUDIOCLIENT.SocketRxBuf ))  ) ) 
            {
            __context__.SourceCodeLine = 714;
            Functions.SocketSend ( AUDIOCLIENT , "\u00FF\u00FC\u0024" ) ; 
            }
        
        __context__.SourceCodeLine = 715;
        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00FF\u00FB\u0003" , AUDIOCLIENT.SocketRxBuf ))  ) ) 
            {
            __context__.SourceCodeLine = 716;
            Functions.SocketSend ( AUDIOCLIENT , "\u00FF\u00FD\u0003" ) ; 
            }
        
        __context__.SourceCodeLine = 717;
        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00FF\u00FD\u0001" , AUDIOCLIENT.SocketRxBuf ))  ) ) 
            {
            __context__.SourceCodeLine = 718;
            Functions.SocketSend ( AUDIOCLIENT , "\u00FF\u00FC\u0001" ) ; 
            }
        
        __context__.SourceCodeLine = 719;
        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00FF\u00FD\u0022" , AUDIOCLIENT.SocketRxBuf ))  ) ) 
            {
            __context__.SourceCodeLine = 720;
            Functions.SocketSend ( AUDIOCLIENT , "\u00FF\u00FC\u0022" ) ; 
            }
        
        __context__.SourceCodeLine = 721;
        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00FF\u00FD\u001F" , AUDIOCLIENT.SocketRxBuf ))  ) ) 
            {
            __context__.SourceCodeLine = 722;
            Functions.SocketSend ( AUDIOCLIENT , "\u00FF\u00FC\u001F" ) ; 
            }
        
        __context__.SourceCodeLine = 723;
        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00FF\u00FB\u0005" , AUDIOCLIENT.SocketRxBuf ))  ) ) 
            {
            __context__.SourceCodeLine = 724;
            Functions.SocketSend ( AUDIOCLIENT , "\u00FF\u00FE\u0005" ) ; 
            }
        
        __context__.SourceCodeLine = 725;
        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00FF\u00FD\u0021" , AUDIOCLIENT.SocketRxBuf ))  ) ) 
            {
            __context__.SourceCodeLine = 726;
            Functions.SocketSend ( AUDIOCLIENT , "\u00FF\u00FC\u0021" ) ; 
            }
        
        __context__.SourceCodeLine = 727;
        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00FF\u00FB\u0001" , AUDIOCLIENT.SocketRxBuf ))  ) ) 
            {
            __context__.SourceCodeLine = 728;
            Functions.SocketSend ( AUDIOCLIENT , "\u00FF\u00FD\u0001" ) ; 
            }
        
        __context__.SourceCodeLine = 729;
        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00FF\u00FD\u0006" , AUDIOCLIENT.SocketRxBuf ))  ) ) 
            {
            __context__.SourceCodeLine = 730;
            Functions.SocketSend ( AUDIOCLIENT , "\u00FF\u00FC\u0006" ) ; 
            }
        
        __context__.SourceCodeLine = 731;
        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00FF\u00FD\u0000" , AUDIOCLIENT.SocketRxBuf ))  ) ) 
            {
            __context__.SourceCodeLine = 732;
            Functions.SocketSend ( AUDIOCLIENT , "\u00FF\u00FC\u0000" ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 733;
            if ( Functions.TestForTrue  ( ( Functions.Find( "Welcome" , AUDIOCLIENT.SocketRxBuf ))  ) ) 
                { 
                __context__.SourceCodeLine = 735;
                SETDEBUG (  __context__ , AUDIOCLIENT.SocketRxBuf, (ushort)( 1 )) ; 
                __context__.SourceCodeLine = 736;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_18__" , 500 , __SPLS_TMPVAR__WAITLABEL_18___Callback ) ;
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 741;
                AUDIODEVICE . RXQUEUE  .UpdateValue ( AUDIODEVICE . RXQUEUE + AUDIOCLIENT .  SocketRxBuf  ) ; 
                __context__.SourceCodeLine = 742;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "\u000D\u000A" , AUDIODEVICE.RXQUEUE ) ) && Functions.TestForTrue ( Functions.Not( AUDIODEVICE.RXQUEUEING ) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 743;
                    PARSEFEEDBACK (  __context__  ) ; 
                    }
                
                } 
            
            }
        
        __context__.SourceCodeLine = 745;
        Functions.ClearBuffer ( AUDIOCLIENT .  SocketRxBuf ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

public void __SPLS_TMPVAR__WAITLABEL_18___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 737;
            DEVICEONLINE (  __context__  ) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object CONNECT_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 749;
        if ( Functions.TestForTrue  ( ( CONNECT  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 751;
            AUDIODEVICE . STATUSCONNECTREQUEST = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 752;
            CONNECTDISCONNECT (  __context__ , (ushort)( 1 )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 756;
            AUDIODEVICE . STATUSCONNECTREQUEST = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 757;
            CONNECTDISCONNECT (  __context__ , (ushort)( 0 )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POLL_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVCOUNTER = 0;
        
        
        __context__.SourceCodeLine = 764;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)64; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 766;
            POLLINSTANCE (  __context__ , (ushort)( LVCOUNTER )) ; 
            __context__.SourceCodeLine = 764;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object RX_OnChange_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 772;
        AUDIODEVICE . RXQUEUE  .UpdateValue ( AUDIODEVICE . RXQUEUE + RX  ) ; 
        __context__.SourceCodeLine = 773;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "\u000D\u000A" , AUDIODEVICE.RXQUEUE ) ) && Functions.TestForTrue ( Functions.Not( AUDIODEVICE.RXQUEUEING ) )) ))  ) ) 
            {
            __context__.SourceCodeLine = 774;
            PARSEFEEDBACK (  __context__  ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object MANUALCMD_OnChange_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 778;
        SETQUEUE (  __context__ , MANUALCMD) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_ADDRESS_OnChange_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 782;
        AUDIODEVICE . IPADDRESS  .UpdateValue ( IP_ADDRESS  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_PORT_OnChange_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 786;
        AUDIODEVICE . IPPORT = (ushort) ( IP_PORT  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LOGIN_NAME_OnChange_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 790;
        AUDIODEVICE . LOGINNAME  .UpdateValue ( LOGIN_NAME  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LOGIN_PASSWORD_OnChange_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 794;
        AUDIODEVICE . LOGINPASSWORD  .UpdateValue ( LOGIN_PASSWORD  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object INSTANCETAGS_OnChange_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 800;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 801;
        LVSTRING  .UpdateValue ( INSTANCETAGS [ LVINDEX ]  ) ; 
        __context__.SourceCodeLine = 802;
        if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVSTRING ))  ) ) 
            { 
            __context__.SourceCodeLine = 804;
            AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ]  .UpdateValue ( Functions.Remove ( "-" , LVSTRING )  ) ; 
            __context__.SourceCodeLine = 805;
            AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ]  .UpdateValue ( Functions.Remove ( (Functions.Length( AUDIODEVICE.INSTANCETAGSNAME[ LVINDEX ] ) - 1), AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] )  ) ; 
            __context__.SourceCodeLine = 806;
            if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVSTRING ))  ) ) 
                { 
                __context__.SourceCodeLine = 808;
                AUDIODEVICE . INSTANCETAGSINDEX [ LVINDEX] = (ushort) ( Functions.Atoi( Functions.Remove( "-" , LVSTRING ) ) ) ; 
                __context__.SourceCodeLine = 809;
                if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVSTRING ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 811;
                    AUDIODEVICE . INSTANCETAGSINDEXSECOND [ LVINDEX] = (ushort) ( Functions.Atoi( Functions.Remove( "-" , LVSTRING ) ) ) ; 
                    __context__.SourceCodeLine = 812;
                    AUDIODEVICE . INSTANCETAGSTYPE [ LVINDEX ]  .UpdateValue ( LVSTRING  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 815;
                    AUDIODEVICE . INSTANCETAGSTYPE [ LVINDEX ]  .UpdateValue ( LVSTRING  ) ; 
                    }
                
                } 
            
            __context__.SourceCodeLine = 817;
            if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSCOMMUNICATING)  ) ) 
                { 
                __context__.SourceCodeLine = 819;
                INITIALIZEINSTANCE (  __context__ , (ushort)( LVINDEX )) ; 
                __context__.SourceCodeLine = 820;
                POLLINSTANCE (  __context__ , (ushort)( LVINDEX )) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEUP_OnPush_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 826;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 827;
        while ( Functions.TestForTrue  ( ( VOLUMEUP[ AUDIODEVICE.LASTINDEX ] .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 829;
            AUDIODEVICE . VOLUMEINUSE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 830;
            CreateWait ( "VOLUP" , 20 , VOLUP_Callback ) ;
            __context__.SourceCodeLine = 827;
            } 
        
        __context__.SourceCodeLine = 860;
        AUDIODEVICE . VOLUMEINUSE = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void VOLUP_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
            
            __context__.SourceCodeLine = 833;
            if ( Functions.TestForTrue  ( ( Functions.Not( Functions.BoolToInt ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] + AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) > AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) ))  ) ) 
                { 
                __context__.SourceCodeLine = 835;
                AUDIODEVICE . INTERNALVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] + AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) ) ; 
                __context__.SourceCodeLine = 836;
                MakeString ( LVSTRING , "{0:d}", (short)AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ]) ; 
                __context__.SourceCodeLine = 837;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 838;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set level " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 839;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        {
                        __context__.SourceCodeLine = 840;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set levelOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                        }
                    
                    }
                
                __context__.SourceCodeLine = 841;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 842;
                AUDIODEVICE . STATUSVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 846;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] != AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ]))  ) ) 
                    { 
                    __context__.SourceCodeLine = 848;
                    AUDIODEVICE . INTERNALVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) ; 
                    __context__.SourceCodeLine = 849;
                    MakeString ( LVSTRING , "{0:d}", (short)AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ]) ; 
                    __context__.SourceCodeLine = 850;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                        {
                        __context__.SourceCodeLine = 851;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set level " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 852;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            {
                            __context__.SourceCodeLine = 853;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set levelOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                            }
                        
                        }
                    
                    __context__.SourceCodeLine = 854;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    __context__.SourceCodeLine = 855;
                    AUDIODEVICE . STATUSVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] ) ; 
                    } 
                
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object VOLUMEDOWN_OnPush_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 864;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 865;
        while ( Functions.TestForTrue  ( ( VOLUMEDOWN[ AUDIODEVICE.LASTINDEX ] .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 867;
            AUDIODEVICE . VOLUMEINUSE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 868;
            CreateWait ( "VOLDN" , 20 , VOLDN_Callback ) ;
            __context__.SourceCodeLine = 865;
            } 
        
        __context__.SourceCodeLine = 899;
        AUDIODEVICE . VOLUMEINUSE = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void VOLDN_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
            
            __context__.SourceCodeLine = 871;
            if ( Functions.TestForTrue  ( ( Functions.Not( Functions.BoolToInt ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] - AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) < AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) ))  ) ) 
                { 
                __context__.SourceCodeLine = 873;
                AUDIODEVICE . INTERNALVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] - AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) ) ; 
                __context__.SourceCodeLine = 874;
                MakeString ( LVSTRING , "{0:d}", (short)AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ]) ; 
                __context__.SourceCodeLine = 875;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 876;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set level " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 877;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        {
                        __context__.SourceCodeLine = 878;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set levelOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                        }
                    
                    }
                
                __context__.SourceCodeLine = 879;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 880;
                AUDIODEVICE . STATUSVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 885;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] != AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ]))  ) ) 
                    { 
                    __context__.SourceCodeLine = 887;
                    AUDIODEVICE . INTERNALVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) ; 
                    __context__.SourceCodeLine = 888;
                    MakeString ( LVSTRING , "{0:d}", (short)AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ]) ; 
                    __context__.SourceCodeLine = 889;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                        {
                        __context__.SourceCodeLine = 890;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set level " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 891;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            {
                            __context__.SourceCodeLine = 892;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set levelOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                            }
                        
                        }
                    
                    __context__.SourceCodeLine = 893;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    __context__.SourceCodeLine = 894;
                    AUDIODEVICE . STATUSVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] ) ; 
                    } 
                
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object VOLUMEMUTEON_OnPush_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 904;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 905;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 907;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set mute " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
            __context__.SourceCodeLine = 908;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 910;
            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 912;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
                __context__.SourceCodeLine = 913;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 914;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                __context__.SourceCodeLine = 915;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 917;
                if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 919;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
                    __context__.SourceCodeLine = 920;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    __context__.SourceCodeLine = 921;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                    __context__.SourceCodeLine = 922;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 924;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 926;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
                        __context__.SourceCodeLine = 927;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        __context__.SourceCodeLine = 928;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                        __context__.SourceCodeLine = 929;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 931;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 933;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set state " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
                            __context__.SourceCodeLine = 934;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            __context__.SourceCodeLine = 935;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get state " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                            __context__.SourceCodeLine = 936;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            } 
                        
                        }
                    
                    }
                
                }
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEMUTEOFF_OnPush_16 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 942;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 943;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 945;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set mute " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
            __context__.SourceCodeLine = 946;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 948;
            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 950;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
                __context__.SourceCodeLine = 951;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 952;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                __context__.SourceCodeLine = 953;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 955;
                if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 957;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
                    __context__.SourceCodeLine = 958;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    __context__.SourceCodeLine = 959;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                    __context__.SourceCodeLine = 960;
                    LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                    __context__.SourceCodeLine = 961;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 963;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 965;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
                        __context__.SourceCodeLine = 966;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        __context__.SourceCodeLine = 967;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                        __context__.SourceCodeLine = 968;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 970;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 972;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set state " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
                            __context__.SourceCodeLine = 973;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            __context__.SourceCodeLine = 974;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get state " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                            __context__.SourceCodeLine = 975;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            } 
                        
                        }
                    
                    }
                
                }
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEMUTETOGGLE_OnPush_17 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 981;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 982;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSVOLUMEMUTE[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 984;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 986;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set mute " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
                __context__.SourceCodeLine = 987;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 989;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 991;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
                    __context__.SourceCodeLine = 992;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    __context__.SourceCodeLine = 993;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                    __context__.SourceCodeLine = 994;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 996;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 998;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
                        __context__.SourceCodeLine = 999;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        __context__.SourceCodeLine = 1000;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                        __context__.SourceCodeLine = 1001;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1003;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1005;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
                            __context__.SourceCodeLine = 1006;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            __context__.SourceCodeLine = 1007;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                            __context__.SourceCodeLine = 1008;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1010;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1012;
                                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set state " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
                                __context__.SourceCodeLine = 1013;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                __context__.SourceCodeLine = 1014;
                                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get state " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                                __context__.SourceCodeLine = 1015;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 1020;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1022;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set mute " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
                __context__.SourceCodeLine = 1023;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1025;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1027;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
                    __context__.SourceCodeLine = 1028;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    __context__.SourceCodeLine = 1029;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                    __context__.SourceCodeLine = 1030;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1032;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1034;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
                        __context__.SourceCodeLine = 1035;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        __context__.SourceCodeLine = 1036;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                        __context__.SourceCodeLine = 1037;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1039;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1041;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
                            __context__.SourceCodeLine = 1042;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            __context__.SourceCodeLine = 1043;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                            __context__.SourceCodeLine = 1044;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1046;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1048;
                                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set state " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
                                __context__.SourceCodeLine = 1049;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                __context__.SourceCodeLine = 1050;
                                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get state " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                                __context__.SourceCodeLine = 1051;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMESET_OnChange_18 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        short LVVOL = 0;
        
        
        __context__.SourceCodeLine = 1059;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1060;
        LVVOL = (short) ( VOLUMECONVERTER( __context__ , (short)( AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) , (short)( AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) , (ushort)( VOLUMESET[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ) ) ; 
        __context__.SourceCodeLine = 1061;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( LVVOL >= AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVVOL <= AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 1063;
            MakeString ( LVSTRING , "{0:d}", (short)LVVOL) ; 
            __context__.SourceCodeLine = 1064;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                {
                __context__.SourceCodeLine = 1065;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set level " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 1066;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    {
                    __context__.SourceCodeLine = 1067;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set levelOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 1068;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object GROUP_OnChange_19 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVCOUNTER = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1075;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1076;
        if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
            { 
            __context__.SourceCodeLine = 1078;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set group " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) )  ) ; 
            __context__.SourceCodeLine = 1079;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1080;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get group " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
            __context__.SourceCodeLine = 1081;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1082;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)64; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 1084;
                if ( Functions.TestForTrue  ( ( Functions.Find( AUDIODEVICE.INSTANCETAGSNAME[ AUDIODEVICE.LASTINDEX ] , AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1086;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVCOUNTER ] + " get muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) )  ) ; 
                    __context__.SourceCodeLine = 1087;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    } 
                
                __context__.SourceCodeLine = 1082;
                } 
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 1091;
            if ( Functions.TestForTrue  ( ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 1093;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set input " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) )  ) ; 
                __context__.SourceCodeLine = 1094;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 1095;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get input " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                __context__.SourceCodeLine = 1096;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1098;
                if ( Functions.TestForTrue  ( ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1100;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set sourceSelection " + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) )  ) ; 
                    __context__.SourceCodeLine = 1101;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1103;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1105;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) )  ) ; 
                        __context__.SourceCodeLine = 1106;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        __context__.SourceCodeLine = 1107;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                        __context__.SourceCodeLine = 1108;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1110;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1112;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) )  ) ; 
                            __context__.SourceCodeLine = 1113;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            __context__.SourceCodeLine = 1114;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                            __context__.SourceCodeLine = 1115;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1117;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "PRST" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1119;
                                LVSTRING  .UpdateValue ( "DEVICE recallPreset " + Functions.ItoA (  (int) ( (GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue + 1000) ) )  ) ; 
                                __context__.SourceCodeLine = 1120;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMESTEP_OnChange_20 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1125;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1126;
        AUDIODEVICE . VOLUMESTEP [ AUDIODEVICE.LASTINDEX] = (ushort) ( VOLUMESTEP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_AUTOANSWERTOGGLE_OnPush_21 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1131;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1132;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSVOIPAUTOANSWER[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1134;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " set autoAnswer " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " false"  ) ; 
            __context__.SourceCodeLine = 1135;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 1139;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " set autoAnswer " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " true"  ) ; 
            __context__.SourceCodeLine = 1140;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_AUTOANSWERON_OnPush_22 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1146;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1147;
        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " set autoAnswer " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " true"  ) ; 
        __context__.SourceCodeLine = 1148;
        SETQUEUE (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_AUTOANSWEROFF_OnPush_23 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1153;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1154;
        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " set autoAnswer " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " false"  ) ; 
        __context__.SourceCodeLine = 1155;
        SETQUEUE (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DNDTOGGLE_OnPush_24 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1160;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1161;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSVOIPDND[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1163;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " set dndEnable " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " false"  ) ; 
            __context__.SourceCodeLine = 1164;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 1168;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " set dndEnable " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " true"  ) ; 
            __context__.SourceCodeLine = 1169;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DNDON_OnPush_25 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1175;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1176;
        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " set dndEnable " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " true"  ) ; 
        __context__.SourceCodeLine = 1177;
        SETQUEUE (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DNDOFF_OnPush_26 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1182;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1183;
        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " set dndEnable " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " false"  ) ; 
        __context__.SourceCodeLine = 1184;
        SETQUEUE (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_ACCEPT_OnPush_27 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1189;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1190;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1192;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " answer " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
            __context__.SourceCodeLine = 1193;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DECLINE_OnPush_28 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1199;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1200;
        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " end " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
        __context__.SourceCodeLine = 1201;
        SETQUEUE (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_JOIN_OnPush_29 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1206;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1207;
        AUDIODEVICE . STATUSVOIPAPPEARANCE [ AUDIODEVICE.LASTINDEX] = (ushort) ( (AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] + 1) ) ; 
        __context__.SourceCodeLine = 1208;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] > 6 ))  ) ) 
            {
            __context__.SourceCodeLine = 1209;
            AUDIODEVICE . STATUSVOIPAPPEARANCE [ AUDIODEVICE.LASTINDEX] = (ushort) ( 1 ) ; 
            }
        
        __context__.SourceCodeLine = 1210;
        VOIPUPDATEAPPEARANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_CONFERENCE_OnPush_30 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1215;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1216;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1218;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " lconf " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
            __context__.SourceCodeLine = 1219;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_REDIAL_OnPush_31 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1225;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1226;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1228;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " redial " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
            __context__.SourceCodeLine = 1229;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_0_OnPush_32 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1234;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "0") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_1_OnPush_33 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1238;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "1") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_2_OnPush_34 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1242;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "2") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_3_OnPush_35 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1246;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "3") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_4_OnPush_36 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1250;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "4") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_5_OnPush_37 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1254;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "5") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_6_OnPush_38 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1258;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "6") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_7_OnPush_39 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1262;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "7") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_8_OnPush_40 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1266;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "8") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_9_OnPush_41 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1270;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "9") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_STAR_OnPush_42 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1274;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "*") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_POUND_OnPush_43 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        uint LVVOL = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 32, this );
        
        
        __context__.SourceCodeLine = 1280;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "#") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_BACKSPACE_OnPush_44 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DIAL_OnPush_45 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1288;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1289;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1291;
            if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSVOIPCALLSTATEAPPEARANCE[ AUDIODEVICE.LASTINDEX , AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 1293;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.VOIPDIALSTRING[ AUDIODEVICE.LASTINDEX ] ) > 1 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1295;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " dial " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] ) ) + AUDIODEVICE . VOIPDIALSTRING [ AUDIODEVICE.LASTINDEX ]  ) ; 
                    __context__.SourceCodeLine = 1297;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    } 
                
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_END_OnPush_46 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1305;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1306;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1308;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " onHook " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
            __context__.SourceCodeLine = 1310;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DIALENTRY_OnChange_47 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1316;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1317;
        AUDIODEVICE . VOIPDIALSTRING [ AUDIODEVICE.LASTINDEX ]  .UpdateValue ( VOIP_DIALENTRY [ AUDIODEVICE.LASTINDEX ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_CALLAPPEARANCE_OnChange_48 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1321;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1322;
        AUDIODEVICE . STATUSVOIPAPPEARANCE [ AUDIODEVICE.LASTINDEX] = (ushort) ( VOIP_CALLAPPEARANCE[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ; 
        __context__.SourceCodeLine = 1323;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] > 6 ))  ) ) 
            {
            __context__.SourceCodeLine = 1324;
            AUDIODEVICE . STATUSVOIPAPPEARANCE [ AUDIODEVICE.LASTINDEX] = (ushort) ( 1 ) ; 
            }
        
        __context__.SourceCodeLine = 1325;
        VOIPUPDATEAPPEARANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    ushort LVCOUNTER = 0;
    
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 1333;
        GVVOIPCOUNTER = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1334;
        GVQUEUEPACER = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1335;
        AUDIODEVICE . IPPORT = (ushort) ( 23 ) ; 
        __context__.SourceCodeLine = 1336;
        AUDIODEVICE . BAUD = (uint) ( 9600 ) ; 
        __context__.SourceCodeLine = 1337;
        AUDIODEVICE . LOGINNAME  .UpdateValue ( "default"  ) ; 
        __context__.SourceCodeLine = 1338;
        AUDIODEVICE . ETX  .UpdateValue ( "\u000D"  ) ; 
        __context__.SourceCodeLine = 1339;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)64; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 1341;
            AUDIODEVICE . VOLUMEMIN [ LVCOUNTER] = (short) ( Functions.ToInteger( -( 40 ) ) ) ; 
            __context__.SourceCodeLine = 1342;
            AUDIODEVICE . VOLUMEMAX [ LVCOUNTER] = (short) ( 10 ) ; 
            __context__.SourceCodeLine = 1343;
            AUDIODEVICE . VOLUMESTEP [ LVCOUNTER] = (ushort) ( 3 ) ; 
            __context__.SourceCodeLine = 1339;
            } 
        
        __context__.SourceCodeLine = 1345;
        ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__2 = (ushort)2; 
        int __FN_FORSTEP_VAL__2 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
            {
            __context__.SourceCodeLine = 1346;
            AUDIODEVICE . STATUSVOIPAPPEARANCE [ LVCOUNTER] = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 1345;
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    AUDIOCLIENT  = new SplusTcpClient ( 65534, this );
    AUDIODEVICE  = new SAUDIO( this, true );
    AUDIODEVICE .PopulateCustomAttributeList( false );
    
    CONNECT = new Crestron.Logos.SplusObjects.DigitalInput( CONNECT__DigitalInput__, this );
    m_DigitalInputList.Add( CONNECT__DigitalInput__, CONNECT );
    
    POLL = new Crestron.Logos.SplusObjects.DigitalInput( POLL__DigitalInput__, this );
    m_DigitalInputList.Add( POLL__DigitalInput__, POLL );
    
    DEBUG = new Crestron.Logos.SplusObjects.DigitalInput( DEBUG__DigitalInput__, this );
    m_DigitalInputList.Add( DEBUG__DigitalInput__, DEBUG );
    
    VOLUMEUP = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEUP[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEUP__DigitalInput__ + i, VOLUMEUP__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEUP__DigitalInput__ + i, VOLUMEUP[i+1] );
    }
    
    VOLUMEDOWN = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEDOWN[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEDOWN__DigitalInput__ + i, VOLUMEDOWN__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEDOWN__DigitalInput__ + i, VOLUMEDOWN[i+1] );
    }
    
    VOLUMEMUTETOGGLE = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTETOGGLE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTETOGGLE__DigitalInput__ + i, VOLUMEMUTETOGGLE__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTETOGGLE__DigitalInput__ + i, VOLUMEMUTETOGGLE[i+1] );
    }
    
    VOLUMEMUTEON = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTEON[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTEON__DigitalInput__ + i, VOLUMEMUTEON__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTEON__DigitalInput__ + i, VOLUMEMUTEON[i+1] );
    }
    
    VOLUMEMUTEOFF = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTEOFF[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTEOFF__DigitalInput__ + i, VOLUMEMUTEOFF__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTEOFF__DigitalInput__ + i, VOLUMEMUTEOFF[i+1] );
    }
    
    VOIP_NUM_0 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_0[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_0__DigitalInput__ + i, VOIP_NUM_0__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_0__DigitalInput__ + i, VOIP_NUM_0[i+1] );
    }
    
    VOIP_NUM_1 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_1[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_1__DigitalInput__ + i, VOIP_NUM_1__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_1__DigitalInput__ + i, VOIP_NUM_1[i+1] );
    }
    
    VOIP_NUM_2 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_2[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_2__DigitalInput__ + i, VOIP_NUM_2__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_2__DigitalInput__ + i, VOIP_NUM_2[i+1] );
    }
    
    VOIP_NUM_3 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_3[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_3__DigitalInput__ + i, VOIP_NUM_3__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_3__DigitalInput__ + i, VOIP_NUM_3[i+1] );
    }
    
    VOIP_NUM_4 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_4[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_4__DigitalInput__ + i, VOIP_NUM_4__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_4__DigitalInput__ + i, VOIP_NUM_4[i+1] );
    }
    
    VOIP_NUM_5 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_5[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_5__DigitalInput__ + i, VOIP_NUM_5__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_5__DigitalInput__ + i, VOIP_NUM_5[i+1] );
    }
    
    VOIP_NUM_6 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_6[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_6__DigitalInput__ + i, VOIP_NUM_6__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_6__DigitalInput__ + i, VOIP_NUM_6[i+1] );
    }
    
    VOIP_NUM_7 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_7[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_7__DigitalInput__ + i, VOIP_NUM_7__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_7__DigitalInput__ + i, VOIP_NUM_7[i+1] );
    }
    
    VOIP_NUM_8 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_8[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_8__DigitalInput__ + i, VOIP_NUM_8__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_8__DigitalInput__ + i, VOIP_NUM_8[i+1] );
    }
    
    VOIP_NUM_9 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_9[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_9__DigitalInput__ + i, VOIP_NUM_9__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_9__DigitalInput__ + i, VOIP_NUM_9[i+1] );
    }
    
    VOIP_NUM_STAR = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_STAR[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_STAR__DigitalInput__ + i, VOIP_NUM_STAR__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_STAR__DigitalInput__ + i, VOIP_NUM_STAR[i+1] );
    }
    
    VOIP_NUM_POUND = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_POUND[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_POUND__DigitalInput__ + i, VOIP_NUM_POUND__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_POUND__DigitalInput__ + i, VOIP_NUM_POUND[i+1] );
    }
    
    VOIP_BACKSPACE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_BACKSPACE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_BACKSPACE__DigitalInput__ + i, VOIP_BACKSPACE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_BACKSPACE__DigitalInput__ + i, VOIP_BACKSPACE[i+1] );
    }
    
    VOIP_AUTOANSWERTOGGLE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWERTOGGLE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_AUTOANSWERTOGGLE__DigitalInput__ + i, VOIP_AUTOANSWERTOGGLE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_AUTOANSWERTOGGLE__DigitalInput__ + i, VOIP_AUTOANSWERTOGGLE[i+1] );
    }
    
    VOIP_AUTOANSWERON = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWERON[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_AUTOANSWERON__DigitalInput__ + i, VOIP_AUTOANSWERON__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_AUTOANSWERON__DigitalInput__ + i, VOIP_AUTOANSWERON[i+1] );
    }
    
    VOIP_AUTOANSWEROFF = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWEROFF[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_AUTOANSWEROFF__DigitalInput__ + i, VOIP_AUTOANSWEROFF__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_AUTOANSWEROFF__DigitalInput__ + i, VOIP_AUTOANSWEROFF[i+1] );
    }
    
    VOIP_DNDTOGGLE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DNDTOGGLE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DNDTOGGLE__DigitalInput__ + i, VOIP_DNDTOGGLE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DNDTOGGLE__DigitalInput__ + i, VOIP_DNDTOGGLE[i+1] );
    }
    
    VOIP_DNDON = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DNDON[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DNDON__DigitalInput__ + i, VOIP_DNDON__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DNDON__DigitalInput__ + i, VOIP_DNDON[i+1] );
    }
    
    VOIP_DNDOFF = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DNDOFF[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DNDOFF__DigitalInput__ + i, VOIP_DNDOFF__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DNDOFF__DigitalInput__ + i, VOIP_DNDOFF[i+1] );
    }
    
    VOIP_DIAL = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DIAL[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DIAL__DigitalInput__ + i, VOIP_DIAL__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DIAL__DigitalInput__ + i, VOIP_DIAL[i+1] );
    }
    
    VOIP_END = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_END[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_END__DigitalInput__ + i, VOIP_END__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_END__DigitalInput__ + i, VOIP_END[i+1] );
    }
    
    VOIP_ACCEPT = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_ACCEPT[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_ACCEPT__DigitalInput__ + i, VOIP_ACCEPT__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_ACCEPT__DigitalInput__ + i, VOIP_ACCEPT[i+1] );
    }
    
    VOIP_DECLINE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DECLINE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DECLINE__DigitalInput__ + i, VOIP_DECLINE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DECLINE__DigitalInput__ + i, VOIP_DECLINE[i+1] );
    }
    
    VOIP_JOIN = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_JOIN[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_JOIN__DigitalInput__ + i, VOIP_JOIN__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_JOIN__DigitalInput__ + i, VOIP_JOIN[i+1] );
    }
    
    VOIP_CONFERENCE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CONFERENCE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_CONFERENCE__DigitalInput__ + i, VOIP_CONFERENCE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_CONFERENCE__DigitalInput__ + i, VOIP_CONFERENCE[i+1] );
    }
    
    VOIP_REDIAL = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_REDIAL[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_REDIAL__DigitalInput__ + i, VOIP_REDIAL__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_REDIAL__DigitalInput__ + i, VOIP_REDIAL[i+1] );
    }
    
    CONNECT_FB = new Crestron.Logos.SplusObjects.DigitalOutput( CONNECT_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( CONNECT_FB__DigitalOutput__, CONNECT_FB );
    
    VOLUMEMUTE_FB = new InOutArray<DigitalOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTE_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOLUMEMUTE_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOLUMEMUTE_FB__DigitalOutput__ + i, VOLUMEMUTE_FB[i+1] );
    }
    
    VOIP_CALLSTATUS_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLSTATUS_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_CALLSTATUS_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_CALLSTATUS_FB__DigitalOutput__ + i, VOIP_CALLSTATUS_FB[i+1] );
    }
    
    VOIP_CALLINCOMING_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLINCOMING_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_CALLINCOMING_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_CALLINCOMING_FB__DigitalOutput__ + i, VOIP_CALLINCOMING_FB[i+1] );
    }
    
    VOIP_AUTOANSWER_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWER_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_AUTOANSWER_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_AUTOANSWER_FB__DigitalOutput__ + i, VOIP_AUTOANSWER_FB[i+1] );
    }
    
    VOIP_DND_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DND_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_DND_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_DND_FB__DigitalOutput__ + i, VOIP_DND_FB[i+1] );
    }
    
    IP_PORT = new Crestron.Logos.SplusObjects.AnalogInput( IP_PORT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( IP_PORT__AnalogSerialInput__, IP_PORT );
    
    VOLUMESET = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMESET[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( VOLUMESET__AnalogSerialInput__ + i, VOLUMESET__AnalogSerialInput__, this );
        m_AnalogInputList.Add( VOLUMESET__AnalogSerialInput__ + i, VOLUMESET[i+1] );
    }
    
    VOLUMESTEP = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMESTEP[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( VOLUMESTEP__AnalogSerialInput__ + i, VOLUMESTEP__AnalogSerialInput__, this );
        m_AnalogInputList.Add( VOLUMESTEP__AnalogSerialInput__ + i, VOLUMESTEP[i+1] );
    }
    
    GROUP = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        GROUP[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( GROUP__AnalogSerialInput__ + i, GROUP__AnalogSerialInput__, this );
        m_AnalogInputList.Add( GROUP__AnalogSerialInput__ + i, GROUP[i+1] );
    }
    
    VOIP_CALLAPPEARANCE = new InOutArray<AnalogInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLAPPEARANCE[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( VOIP_CALLAPPEARANCE__AnalogSerialInput__ + i, VOIP_CALLAPPEARANCE__AnalogSerialInput__, this );
        m_AnalogInputList.Add( VOIP_CALLAPPEARANCE__AnalogSerialInput__ + i, VOIP_CALLAPPEARANCE[i+1] );
    }
    
    CONNECT_STATUS_FB = new Crestron.Logos.SplusObjects.AnalogOutput( CONNECT_STATUS_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CONNECT_STATUS_FB__AnalogSerialOutput__, CONNECT_STATUS_FB );
    
    VOLUMELEVEL_FB = new InOutArray<AnalogOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMELEVEL_FB[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( VOLUMELEVEL_FB__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( VOLUMELEVEL_FB__AnalogSerialOutput__ + i, VOLUMELEVEL_FB[i+1] );
    }
    
    GROUP_FB = new InOutArray<AnalogOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        GROUP_FB[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( GROUP_FB__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( GROUP_FB__AnalogSerialOutput__ + i, GROUP_FB[i+1] );
    }
    
    IP_ADDRESS = new Crestron.Logos.SplusObjects.StringInput( IP_ADDRESS__AnalogSerialInput__, 16, this );
    m_StringInputList.Add( IP_ADDRESS__AnalogSerialInput__, IP_ADDRESS );
    
    LOGIN_NAME = new Crestron.Logos.SplusObjects.StringInput( LOGIN_NAME__AnalogSerialInput__, 32, this );
    m_StringInputList.Add( LOGIN_NAME__AnalogSerialInput__, LOGIN_NAME );
    
    LOGIN_PASSWORD = new Crestron.Logos.SplusObjects.StringInput( LOGIN_PASSWORD__AnalogSerialInput__, 32, this );
    m_StringInputList.Add( LOGIN_PASSWORD__AnalogSerialInput__, LOGIN_PASSWORD );
    
    RX = new Crestron.Logos.SplusObjects.StringInput( RX__AnalogSerialInput__, 511, this );
    m_StringInputList.Add( RX__AnalogSerialInput__, RX );
    
    MANUALCMD = new Crestron.Logos.SplusObjects.StringInput( MANUALCMD__AnalogSerialInput__, 127, this );
    m_StringInputList.Add( MANUALCMD__AnalogSerialInput__, MANUALCMD );
    
    INSTANCETAGS = new InOutArray<StringInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        INSTANCETAGS[i+1] = new Crestron.Logos.SplusObjects.StringInput( INSTANCETAGS__AnalogSerialInput__ + i, INSTANCETAGS__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( INSTANCETAGS__AnalogSerialInput__ + i, INSTANCETAGS[i+1] );
    }
    
    VOIP_DIALENTRY = new InOutArray<StringInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DIALENTRY[i+1] = new Crestron.Logos.SplusObjects.StringInput( VOIP_DIALENTRY__AnalogSerialInput__ + i, VOIP_DIALENTRY__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( VOIP_DIALENTRY__AnalogSerialInput__ + i, VOIP_DIALENTRY[i+1] );
    }
    
    TX = new Crestron.Logos.SplusObjects.StringOutput( TX__AnalogSerialOutput__, this );
    m_StringOutputList.Add( TX__AnalogSerialOutput__, TX );
    
    VOIP_DIALSTRING = new InOutArray<StringOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DIALSTRING[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VOIP_DIALSTRING__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VOIP_DIALSTRING__AnalogSerialOutput__ + i, VOIP_DIALSTRING[i+1] );
    }
    
    VOIP_CALLINCOMINGNAME_FB = new InOutArray<StringOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLINCOMINGNAME_FB[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VOIP_CALLINCOMINGNAME_FB__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VOIP_CALLINCOMINGNAME_FB__AnalogSerialOutput__ + i, VOIP_CALLINCOMINGNAME_FB[i+1] );
    }
    
    VOIP_CALLINCOMINGNUM = new InOutArray<StringOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLINCOMINGNUM[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VOIP_CALLINCOMINGNUM__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VOIP_CALLINCOMINGNUM__AnalogSerialOutput__ + i, VOIP_CALLINCOMINGNUM[i+1] );
    }
    
    UNIQUE_ID = new UShortParameter( UNIQUE_ID__Parameter__, this );
    m_ParameterList.Add( UNIQUE_ID__Parameter__, UNIQUE_ID );
    
    __SPLS_TMPVAR__WAITLABEL_16___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_16___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_17___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_17___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_18___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_18___CallbackFn );
    VOLUP_Callback = new WaitFunction( VOLUP_CallbackFn );
    VOLDN_Callback = new WaitFunction( VOLDN_CallbackFn );
    
    AUDIOCLIENT.OnSocketConnect.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketConnect_0, true ) );
    AUDIOCLIENT.OnSocketDisconnect.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketDisconnect_1, true ) );
    AUDIOCLIENT.OnSocketStatus.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketStatus_2, true ) );
    AUDIOCLIENT.OnSocketReceive.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketReceive_3, false ) );
    CONNECT.OnDigitalChange.Add( new InputChangeHandlerWrapper( CONNECT_OnChange_4, false ) );
    POLL.OnDigitalPush.Add( new InputChangeHandlerWrapper( POLL_OnPush_5, false ) );
    RX.OnSerialChange.Add( new InputChangeHandlerWrapper( RX_OnChange_6, false ) );
    MANUALCMD.OnSerialChange.Add( new InputChangeHandlerWrapper( MANUALCMD_OnChange_7, false ) );
    IP_ADDRESS.OnSerialChange.Add( new InputChangeHandlerWrapper( IP_ADDRESS_OnChange_8, false ) );
    IP_PORT.OnAnalogChange.Add( new InputChangeHandlerWrapper( IP_PORT_OnChange_9, false ) );
    LOGIN_NAME.OnSerialChange.Add( new InputChangeHandlerWrapper( LOGIN_NAME_OnChange_10, false ) );
    LOGIN_PASSWORD.OnSerialChange.Add( new InputChangeHandlerWrapper( LOGIN_PASSWORD_OnChange_11, false ) );
    for( uint i = 0; i < 64; i++ )
        INSTANCETAGS[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( INSTANCETAGS_OnChange_12, true ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEUP[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEUP_OnPush_13, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEDOWN[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEDOWN_OnPush_14, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEMUTEON[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTEON_OnPush_15, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEMUTEOFF[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTEOFF_OnPush_16, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEMUTETOGGLE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTETOGGLE_OnPush_17, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMESET[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( VOLUMESET_OnChange_18, false ) );
        
    for( uint i = 0; i < 64; i++ )
        GROUP[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( GROUP_OnChange_19, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMESTEP[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( VOLUMESTEP_OnChange_20, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_AUTOANSWERTOGGLE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_AUTOANSWERTOGGLE_OnPush_21, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_AUTOANSWERON[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_AUTOANSWERON_OnPush_22, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_AUTOANSWEROFF[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_AUTOANSWEROFF_OnPush_23, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DNDTOGGLE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DNDTOGGLE_OnPush_24, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DNDON[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DNDON_OnPush_25, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DNDOFF[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DNDOFF_OnPush_26, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_ACCEPT[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_ACCEPT_OnPush_27, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DECLINE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DECLINE_OnPush_28, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_JOIN[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_JOIN_OnPush_29, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_CONFERENCE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_CONFERENCE_OnPush_30, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_REDIAL[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_REDIAL_OnPush_31, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_0[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_0_OnPush_32, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_1[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_1_OnPush_33, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_2[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_2_OnPush_34, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_3[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_3_OnPush_35, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_4[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_4_OnPush_36, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_5[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_5_OnPush_37, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_6[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_6_OnPush_38, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_7[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_7_OnPush_39, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_8[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_8_OnPush_40, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_9[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_9_OnPush_41, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_STAR[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_STAR_OnPush_42, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_POUND[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_POUND_OnPush_43, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_BACKSPACE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_BACKSPACE_OnPush_44, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DIAL[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DIAL_OnPush_45, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_END[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_END_OnPush_46, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DIALENTRY[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( VOIP_DIALENTRY_OnChange_47, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_CALLAPPEARANCE[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( VOIP_CALLAPPEARANCE_OnChange_48, false ) );
        
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_DSP_BIAMP_TESIRA_V0_3 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_16___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_17___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_18___Callback;
private WaitFunction VOLUP_Callback;
private WaitFunction VOLDN_Callback;


const uint CONNECT__DigitalInput__ = 0;
const uint POLL__DigitalInput__ = 1;
const uint DEBUG__DigitalInput__ = 2;
const uint IP_PORT__AnalogSerialInput__ = 0;
const uint IP_ADDRESS__AnalogSerialInput__ = 1;
const uint LOGIN_NAME__AnalogSerialInput__ = 2;
const uint LOGIN_PASSWORD__AnalogSerialInput__ = 3;
const uint RX__AnalogSerialInput__ = 4;
const uint MANUALCMD__AnalogSerialInput__ = 5;
const uint VOLUMEUP__DigitalInput__ = 3;
const uint VOLUMEDOWN__DigitalInput__ = 67;
const uint VOLUMEMUTETOGGLE__DigitalInput__ = 131;
const uint VOLUMEMUTEON__DigitalInput__ = 195;
const uint VOLUMEMUTEOFF__DigitalInput__ = 259;
const uint VOIP_NUM_0__DigitalInput__ = 323;
const uint VOIP_NUM_1__DigitalInput__ = 325;
const uint VOIP_NUM_2__DigitalInput__ = 327;
const uint VOIP_NUM_3__DigitalInput__ = 329;
const uint VOIP_NUM_4__DigitalInput__ = 331;
const uint VOIP_NUM_5__DigitalInput__ = 333;
const uint VOIP_NUM_6__DigitalInput__ = 335;
const uint VOIP_NUM_7__DigitalInput__ = 337;
const uint VOIP_NUM_8__DigitalInput__ = 339;
const uint VOIP_NUM_9__DigitalInput__ = 341;
const uint VOIP_NUM_STAR__DigitalInput__ = 343;
const uint VOIP_NUM_POUND__DigitalInput__ = 345;
const uint VOIP_BACKSPACE__DigitalInput__ = 347;
const uint VOIP_AUTOANSWERTOGGLE__DigitalInput__ = 349;
const uint VOIP_AUTOANSWERON__DigitalInput__ = 351;
const uint VOIP_AUTOANSWEROFF__DigitalInput__ = 353;
const uint VOIP_DNDTOGGLE__DigitalInput__ = 355;
const uint VOIP_DNDON__DigitalInput__ = 357;
const uint VOIP_DNDOFF__DigitalInput__ = 359;
const uint VOIP_DIAL__DigitalInput__ = 361;
const uint VOIP_END__DigitalInput__ = 363;
const uint VOIP_ACCEPT__DigitalInput__ = 365;
const uint VOIP_DECLINE__DigitalInput__ = 367;
const uint VOIP_JOIN__DigitalInput__ = 369;
const uint VOIP_CONFERENCE__DigitalInput__ = 371;
const uint VOIP_REDIAL__DigitalInput__ = 373;
const uint VOLUMESET__AnalogSerialInput__ = 6;
const uint VOLUMESTEP__AnalogSerialInput__ = 70;
const uint GROUP__AnalogSerialInput__ = 134;
const uint VOIP_CALLAPPEARANCE__AnalogSerialInput__ = 198;
const uint INSTANCETAGS__AnalogSerialInput__ = 200;
const uint VOIP_DIALENTRY__AnalogSerialInput__ = 264;
const uint CONNECT_FB__DigitalOutput__ = 0;
const uint CONNECT_STATUS_FB__AnalogSerialOutput__ = 0;
const uint TX__AnalogSerialOutput__ = 1;
const uint VOLUMEMUTE_FB__DigitalOutput__ = 1;
const uint VOIP_CALLSTATUS_FB__DigitalOutput__ = 65;
const uint VOIP_CALLINCOMING_FB__DigitalOutput__ = 67;
const uint VOIP_AUTOANSWER_FB__DigitalOutput__ = 69;
const uint VOIP_DND_FB__DigitalOutput__ = 71;
const uint VOLUMELEVEL_FB__AnalogSerialOutput__ = 2;
const uint GROUP_FB__AnalogSerialOutput__ = 66;
const uint VOIP_DIALSTRING__AnalogSerialOutput__ = 130;
const uint VOIP_CALLINCOMINGNAME_FB__AnalogSerialOutput__ = 132;
const uint VOIP_CALLINCOMINGNUM__AnalogSerialOutput__ = 134;
const uint UNIQUE_ID__Parameter__ = 10;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SAUDIO : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  STATUSCONNECTREQUEST = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  STATUSCONNECTED = 0;
    
    [SplusStructAttribute(2, false, false)]
    public ushort  STATUSCOMMUNICATING = 0;
    
    [SplusStructAttribute(3, false, false)]
    public ushort  [] STATUSVOLUMEMUTE;
    
    [SplusStructAttribute(4, false, false)]
    public ushort  [] STATUSVOIPAUTOANSWER;
    
    [SplusStructAttribute(5, false, false)]
    public ushort  [] STATUSVOIPDND;
    
    [SplusStructAttribute(6, false, false)]
    public ushort  [,] STATUSVOIPCALLSTATEAPPEARANCE;
    
    [SplusStructAttribute(7, false, false)]
    public ushort  [] STATUSVOIPAPPEARANCE;
    
    [SplusStructAttribute(8, false, false)]
    public CrestronString  [] VOIPDIALSTRING;
    
    [SplusStructAttribute(9, false, false)]
    public ushort  [] VOIPINSTANCEINDEX;
    
    [SplusStructAttribute(10, false, false)]
    public ushort  VOLUMEINUSE = 0;
    
    [SplusStructAttribute(11, false, false)]
    public ushort  LASTINDEX = 0;
    
    [SplusStructAttribute(12, false, false)]
    public ushort  LASTPOLLINDEX = 0;
    
    [SplusStructAttribute(13, false, false)]
    public ushort  LASTPOLLSECONDINDEX = 0;
    
    [SplusStructAttribute(14, false, false)]
    public uint  BAUD = 0;
    
    [SplusStructAttribute(15, false, false)]
    public short  [] STATUSVOLUME;
    
    [SplusStructAttribute(16, false, false)]
    public short  [] INTERNALVOLUME;
    
    [SplusStructAttribute(17, false, false)]
    public CrestronString  ETX;
    
    [SplusStructAttribute(18, false, false)]
    public CrestronString  IPADDRESS;
    
    [SplusStructAttribute(19, false, false)]
    public ushort  IPPORT = 0;
    
    [SplusStructAttribute(20, false, false)]
    public CrestronString  LOGINNAME;
    
    [SplusStructAttribute(21, false, false)]
    public CrestronString  LOGINPASSWORD;
    
    [SplusStructAttribute(22, false, false)]
    public CrestronString  [] INSTANCETAGSNAME;
    
    [SplusStructAttribute(23, false, false)]
    public CrestronString  [] INSTANCETAGSTYPE;
    
    [SplusStructAttribute(24, false, false)]
    public ushort  [] INSTANCETAGSINDEX;
    
    [SplusStructAttribute(25, false, false)]
    public ushort  [] INSTANCETAGSINDEXSECOND;
    
    [SplusStructAttribute(26, false, false)]
    public ushort  [] INSTANCETAGSPOLL;
    
    [SplusStructAttribute(27, false, false)]
    public short  [] VOLUMEMIN;
    
    [SplusStructAttribute(28, false, false)]
    public short  [] VOLUMEMAX;
    
    [SplusStructAttribute(29, false, false)]
    public ushort  [] VOLUMESTEP;
    
    [SplusStructAttribute(30, false, false)]
    public CrestronString  TXQUEUE;
    
    [SplusStructAttribute(31, false, false)]
    public CrestronString  RXQUEUE;
    
    [SplusStructAttribute(32, false, false)]
    public ushort  TXQUEUEING = 0;
    
    [SplusStructAttribute(33, false, false)]
    public ushort  RXQUEUEING = 0;
    
    
    public SAUDIO( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        STATUSVOLUMEMUTE  = new ushort[ 65 ];
        STATUSVOIPAUTOANSWER  = new ushort[ 3 ];
        STATUSVOIPDND  = new ushort[ 3 ];
        STATUSVOIPAPPEARANCE  = new ushort[ 3 ];
        VOIPINSTANCEINDEX  = new ushort[ 3 ];
        INSTANCETAGSINDEX  = new ushort[ 65 ];
        INSTANCETAGSINDEXSECOND  = new ushort[ 65 ];
        INSTANCETAGSPOLL  = new ushort[ 65 ];
        VOLUMESTEP  = new ushort[ 65 ];
        STATUSVOIPCALLSTATEAPPEARANCE  = new ushort[ 3,7 ];
        STATUSVOLUME  = new short[ 65 ];
        INTERNALVOLUME  = new short[ 65 ];
        VOLUMEMIN  = new short[ 65 ];
        VOLUMEMAX  = new short[ 65 ];
        ETX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2, Owner );
        IPADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        LOGINNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        LOGINPASSWORD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        TXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 65534, Owner );
        RXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 65534, Owner );
        VOIPDIALSTRING  = new CrestronString[ 3 ];
        for( uint i = 0; i < 3; i++ )
            VOIPDIALSTRING [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        INSTANCETAGSNAME  = new CrestronString[ 65 ];
        for( uint i = 0; i < 65; i++ )
            INSTANCETAGSNAME [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        INSTANCETAGSTYPE  = new CrestronString[ 65 ];
        for( uint i = 0; i < 65; i++ )
            INSTANCETAGSTYPE [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        
        
    }
    
}

}
