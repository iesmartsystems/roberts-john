/*
Dealer Name: i.e.SmartSytems
System Name: Link MX88-4kUHDE v0_1
System Number:
Programmer: Jesus Barrera
Comments: Link MX88-4kUHDE v0_1
*/
/*****************************************************************************************************************************
    i.e.SmartSystems LLC CONFIDENTIAL
    Copyright (c) 2011-2016 i.e.SmartSystems LLC, All Rights Reserved.
    NOTICE:  All information contained herein is, and remains the property of i.e.SmartSystems. The intellectual and 
    technical concepts contained herein are proprietary to i.e.SmartSystems and may be covered by U.S. and Foreign Patents, 
    patents in process, and are protected by trade secret or copyright law. Dissemination of this information or 
    reproduction of this material is strictly forbidden unless prior written permission is obtained from i.e.SmartSystems.  
    Access to the source code contained herein is hereby forbidden to anyone except current i.e.SmartSystems employees, 
    managers or contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
    The copyright notice above does not evidence any actual or intended publication or disclosure of this source code, 
    which includes information that is confidential and/or proprietary, and is a trade secret, of i.e.SmartSystems.   
    ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS SOURCE 
    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF i.e.SmartSystems IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE 
    LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY 
    OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  
    MAY DESCRIBE, IN WHOLE OR IN PART.
*****************************************************************************************************************************/
/*******************************************************************************************
  Compiler Directives
*******************************************************************************************/
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_DYNAMIC
#ENABLE_TRACE
#DEFINE_CONSTANT NUM_INPUTS 8
#DEFINE_CONSTANT NUM_OUTPUTS 8
//#HELP_PDF_FILE "IESS Switcher RGB MediaWall V v0_1.pdf"
/*******************************************************************************************
  Include Libraries
*******************************************************************************************/
/*******************************************************************************************
  DIGITAL, ANALOG and SERIAL INPUTS and OUTPUTS
*******************************************************************************************/
DIGITAL_INPUT Connect, Poll, Debug, _SKIP_;
ANALOG_INPUT IP_Port, _SKIP_;
STRING_INPUT IP_Address[31], _SKIP_, RX[127], ManualCMD[127], _SKIP_;
DIGITAL_INPUT Select_Display[NUM_OUTPUTS], _SKIP_, Route_Source[NUM_INPUTS], _SKIP_;
ANALOG_INPUT SourceXtoDisplay[NUM_OUTPUTS];
DIGITAL_OUTPUT Connect_FB, _SKIP_;
ANALOG_OUTPUT Connect_Status_FB, _SKIP_;
STRING_OUTPUT TX, _SKIP_;
ANALOG_OUTPUT SourceXonDisplay[NUM_OUTPUTS];
/*******************************************************************************************
  SOCKETS
*******************************************************************************************/
TCP_CLIENT SwitcherClient[2047];
/*******************************************************************************************
  Parameters
*******************************************************************************************/
/*******************************************************************************************
  Parameter Properties
*******************************************************************************************/
/*******************************************************************************************
  Structure Definitions
*******************************************************************************************/
STRUCTURE sSwitcher
{
	INTEGER StatusConnectRequest;
	INTEGER StatusConnected;
	INTEGER StatusCommunicating;
	STRING ETX[2];
	STRING IPAddress[31];
	INTEGER IPPort;//8000
	STRING TXQueue[1023];
	STRING RXQueue[1023];
};
sSwitcher SwitcherDevice;
/*******************************************************************************************
  Global Variables
*******************************************************************************************/
INTEGER gvDisplay, gvSource;
/*******************************************************************************************
  Functions
*******************************************************************************************/
FUNCTION ConnectDisconnect( INTEGER lvConnect )
{
	SIGNED_INTEGER lvStatus;
	IF( !lvConnect )
	{
		IF( SwitcherDevice.StatusConnected )
			lvStatus = SOCKETDISCONNECTCLIENT( SwitcherClient );
	}
	ELSE IF( lvConnect )
	{
		IF( !SwitcherDevice.StatusConnected )
		{
			IF( LEN( SwitcherDevice.IPAddress ) > 4 && SwitcherDevice.IPPort > 0 )
			{
				IF( SwitcherDevice.StatusConnectRequest )
					lvStatus = SOCKETCONNECTCLIENT( SwitcherClient, SwitcherDevice.IPAddress, SwitcherDevice.IPPort, 1 );
			}
		}
	}
}
FUNCTION SetDebug( STRING lvString, INTEGER lvType )
{
	IF( Debug )
	{
		IF( lvType = 1) //RX
			TRACE( "Link RX: %s", lvString );
		ELSE			//TX
			TRACE( "Link TX: %s", lvString );
	}
}
FUNCTION SetQueue( STRING lvString )
{
	STRING lvTemp[511];
	SwitcherDevice.TXQueue =  SwitcherDevice.TXQueue + lvString + SwitcherDevice.ETX + "\x0B\x0B";
	WHILE( FIND( "\x0B\x0B", SwitcherDevice.TXQueue ) )
	{
		lvTemp = REMOVE( "\x0B\x0B", SwitcherDevice.TXQueue );
		lvTemp = REMOVEBYLENGTH( LEN( lvTemp ) - 2, lvTemp );
		lvTemp = lvTemp + SwitcherDevice.ETX;
		IF( SwitcherDevice.StatusConnectRequest )
		{
			IF( SwitcherDevice.StatusConnected )
				SOCKETSEND( SwitcherClient, lvTemp );
		}
		ELSE
			TX = lvTemp;
		SetDebug( lvTemp, 0 );
	}
}
FUNCTION ParseFeedback()
{
	STRING lvRX[1023], lvTrash[63];
/*
	WHILE( FIND( "\x0D\x0A", SwitcherDevice.RXQueue ) )			//Incoming buffer still has info
	{
		lvRX = REMOVE( "\x0D\x0A", SwitcherDevice.RXQueue );
		lvRX = REMOVEBYLENGTH( LEN( lvRX ) - 2, lvRX );
		SetDebug( lvRX, 1 );
		IF( !SwitcherDevice.StatusCommunicating )
			SwitcherDevice.StatusCommunicating = 1;
		IF( FIND( "SOURCE: ", lvRX ) )
		{
			lvTrash = REMOVE( "SOURCE: ", lvRX );
			IF( SwitcherDevice.LastWindowPoll )
			{
				SwitcherDevice.StatusWindowSource[SwitcherDevice.LastWindowPoll] = ATOI( lvRX );
				Window_Video_FB[SwitcherDevice.LastWindowPoll] = ATOI( lvRX );
			}
		}
	}
*/
}
FUNCTION RouteDisplay()
{
	STRING lvString[127];
	MAKESTRING( lvString, "SET SW HDMIIN%d HDMIOUT%d", gvSource, gvDisplay );	
	SetQueue( lvString );
}
FUNCTION PowerOnDisplay()
{
	STRING lvString[127];
	MAKESTRING( lvString, "SET CEC_PWR HDMIOUT%d ON", gvDisplay );	
	SetQueue( lvString );
}
FUNCTION PowerOffDisplay()
{
	STRING lvString[127];
	MAKESTRING( lvString, "SET CEC_PWR HDMIOUT%d OFF", gvDisplay );	
	SetQueue( lvString );
}			
/*******************************************************************************************
  Event Handlers
*******************************************************************************************/
//Connected
SOCKETCONNECT SwitcherClient
{
	STRING lvString[63];
	SwitcherDevice.StatusConnected = 1;
	Connect_FB = 1;
	Connect_Status_FB = SwitcherClient.SocketStatus;
}
//Disconnected
SOCKETDISCONNECT SwitcherClient
{
	SwitcherDevice.StatusConnected = 0;
	SwitcherDevice.StatusCommunicating = 0;
	Connect_FB = 0;
	Connect_Status_FB = SwitcherClient.SocketStatus;
}
SOCKETSTATUS SwitcherClient
{ 
	Connect_Status_FB = SOCKETGETSTATUS();
	IF( Connect_Status_FB )
	{
		SwitcherDevice.StatusConnected = 1;
		Connect_FB = 1;
    }
	ELSE
	{
		SwitcherDevice.StatusConnected = 0;
		SwitcherDevice.StatusCommunicating = 0;
		Connect_FB = 0;
	}
}
//Feedback
SOCKETRECEIVE SwitcherClient
{
	SwitcherDevice.RXQueue = SwitcherDevice.RXQueue + SwitcherClient.SocketRxBuf;
	CLEARBUFFER( SwitcherClient.SocketRxBuf );
	IF( LEN( SwitcherDevice.RXQueue ) > 1 )
		ParseFeedback();
}
CHANGE Connect
{
	IF( Connect )
	{	
		SwitcherDevice.StatusConnectRequest = 1;
		ConnectDisconnect( 1 );
	}
	ELSE
	{
		SwitcherDevice.StatusConnectRequest = 0;
		ConnectDisconnect( 0 );
	}					
}
CHANGE IP_Address
{
	SwitcherDevice.IPAddress = IP_Address;
}
CHANGE IP_Port
{
	SwitcherDevice.IPPort = IP_Port;
}
CHANGE RX
{
	SwitcherDevice.RXQueue = SwitcherDevice.RXQueue + RX;
	IF( LEN( SwitcherDevice.RXQueue ) > 1 )
		ParseFeedback();
}
PUSH Poll
{
	INTEGER lvCounter;
	STRING lvString[31];
/*
	FOR( lvCounter = 1 TO SwitcherDevice.NumberOfWindows )
    {
		SwitcherDevice.LastWindowPoll = lvCounter;
		lvString = "winsstat " + ITOA( WallSelect ) + ITOA( lvCounter );
		SetQueue( lvString );
	}
*/
}
CHANGE ManualCMD
{
	SetQueue( ManualCMD );
}
PUSH Select_Display
{
	gvDisplay = GETLASTMODIFIEDARRAYINDEX();
}
PUSH Route_Source
{
	gvSource = GETLASTMODIFIEDARRAYINDEX();
	RouteDisplay();
}
CHANGE SourceXtoDisplay
{
	gvDisplay = GETLASTMODIFIEDARRAYINDEX();
	gvSource = SourceXtoDisplay[gvDisplay];
	RouteDisplay();
	IF( gvSource = 0 | gvSource = 99 )
	{
    	PowerOffDisplay();
	}
	ELSE
	{
		PowerOnDisplay();
	}
}
/*******************************************************************************************
  Main()
*******************************************************************************************/
Function Main()
{
	SwitcherDevice.IPPort = 23;
	SwitcherDevice.IPAddress = "192.168.10.254";
	SwitcherDevice.ETX = "\x0D\x0A";
}
